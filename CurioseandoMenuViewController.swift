//
//  CurioseandoMenuViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 9/3/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import Nuke

class CurioseandoMenuViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var subHeaderView: UIView!
    @IBOutlet weak var menuCollection: UICollectionView!
    var curioItems:[[String: Any]] = []
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    var mGame = Game()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPatterns()
        
        activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        activityIndicator.color = UIColor.lightGray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        self.menuCollection.register(UINib(nibName: "CurioseandoMenuViewCell", bundle: nil), forCellWithReuseIdentifier: "curioCell")
        if let flowLayout = menuCollection.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height:1)
        }
        
        loadMenu()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
        UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.subHeaderView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    func loadMenu() {
        curioItems = []
        
        Alamofire.request(ApiConfig.curioseandoTests, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                        for itemSet in data {
                            if let itemSet = itemSet as? [String: Any] {
                                weakself?.curioItems.append(itemSet)
                            }
                        }
                        
                        DispatchQueue.main.async() {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.activityIndicator.stopAnimating()
                            weakself?.menuCollection.reloadData()
                        }
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == menuCollection {
            return curioItems.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == menuCollection {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let main = storyboard.instantiateViewController(withIdentifier: "inGameId") as! InGameViewController
            let element:[String:Any] = curioItems[indexPath.row]
            self.mGame.id = element["id"] as! String
            main.mGame = self.mGame
            self.present(main, animated: true, completion: nil)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == menuCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "curioCell", for: indexPath) as! CurioseandoMenuViewCell
            
            if(curioItems.count > 0){
                let element:[String:Any] = curioItems[indexPath.row]
                print(element)
                do {
                    let attrStr = try NSAttributedString(
                        data: ((element["nombre"] as? String)?.data(using: String.Encoding.unicode, allowLossyConversion: true)!)!,
                        options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                        documentAttributes: nil)
                    cell.gameTitle.attributedText = attrStr
                } catch let error {
                    
                }
                self.view.layoutIfNeeded()
            }
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "curioCell", for: indexPath) as! CurioseandoMenuViewCell
            return cell
        }
    }
}
