//
//  Respuesta.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 9/3/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import RealmSwift

class Respuesta {
    var id = ""
    var respuesta = ""
    var imagen = ""
    var isCorrecta = ""
    var preguntasNahuatlismosId = ""
    var preguntasCurioseandoId = ""
    var resultados = [Resultado]()
}
