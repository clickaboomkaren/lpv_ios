//
//  GacetitaViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/1/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import Nuke

class GacetitaViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var subHeaderView: UIView!
    @IBOutlet weak var gacetitaCollection: UICollectionView!
    var gacetitaItems:[[String: Any]] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPatterns()
        
        self.gacetitaCollection.register(UINib(nibName: "BookViewCell", bundle: nil), forCellWithReuseIdentifier: "gacetitaCell")
        loadGacetitas(url:ApiConfig.gacetitas, params:"")
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
        UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.subHeaderView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    func loadGacetitas(url:String, params: String) {
        gacetitaItems.removeAll()
        
        Alamofire.request(url + params, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                        for legendsSet in data {
                            if let legendsSet = legendsSet as? [[String: Any]] {
                                for legend in legendsSet {
                                    if let legend = legend as? [String: Any] {
                                        weakself?.gacetitaItems.append(legend)
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async() {
                            weakself?.gacetitaCollection.reloadData()
                        }
                    }
                }
                
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == gacetitaCollection {
            return gacetitaItems.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let element:[String:Any] = gacetitaItems[indexPath.row]
        if let value = element["pdf"] as? String {
            let url:URL = URL(string: ApiConfig.gacetitaPdf + value)!
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            } else {
                UIApplication.shared.openURL(url)
            }
        } else {
            return
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == gacetitaCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gacetitaCell", for: indexPath) as! LeyendasViewCell
            
            cell.bookImg.image = nil
            
            if(gacetitaItems.count > 0){
                let element:[String:Any] = gacetitaItems[indexPath.row]
                print(element)
                if let imagen = element["image"] as? String {
                    //print(image)
                    let url = URL.init(string:ApiConfig.gacetitaImg + imagen)!
                    Nuke.loadImage(with: url, into: cell.bookImg)
                }
                cell.title.text = element["titulo"] as? String
                // Obtener autor
                //let autores:[[String: Any]] = (element["autores"] as? [[String : Any]])!
                //let autor:[String:Any] = autores[0]
                cell.subtitle.text = element["autor"] as? String
                
                self.view.layoutIfNeeded()
                
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "gacetitaCell", for: indexPath) as! LeyendasViewCell
            return cell
        }
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      let height = self.view.frame.size.height
      let width = self.view.frame.size.width
        if collectionView == gacetitaCollection {
         switch UIDevice.current.userInterfaceIdiom {
         case .phone:
            // It's an iPhone
            return CGSize(width: width/3.6, height: height*0.25)
         case .pad:
            // It's an iPad
            return CGSize(width: height*0.2, height: height*0.25)
         case .unspecified:
            // Uh, oh! What could it be?
            return CGSize(width: height*3.6, height: height*0.25)
         default:
            // default
            return CGSize(width: height*3.6, height: height*0.25)
         }
        } else {
            return CGSize(width: self.view.frame.width, height: collectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == gacetitaCollection {
            return 0
        } else {
            return 0
        }
    }
    
}
