//
//  JuegosViewCell.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/28/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class JuegosViewCell: UICollectionViewCell {
    
    @IBOutlet weak var gameImg: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        widthConstraint.constant = screenWidth - (2 * 12)

    }

}
