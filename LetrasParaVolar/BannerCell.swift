//
//  BannerCell.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 7/6/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class BannerCell: UICollectionViewCell {
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var heightConstraint: NSLayoutConstraint!
}
