//
//  BottomNavViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 7/3/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import RealmSwift

class TabBarViewController: UIViewController, FileManagerDelegate {
    
    /*@IBAction func backNavBtnPressed(_ sender: UIBarButtonItem) {
        // Select homeViewController
        selectedIndex = 0
        buttons[selectedIndex].isSelected = true
        didPressTab(buttons[selectedIndex])
    }*/
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        // Select homeViewController
        selectedIndex = 0
        buttons[selectedIndex].isSelected = true
        didPressTab(buttons[selectedIndex])
    }
    /*@IBAction func menuBtnPressed(_ sender: UIButton) {
        // Lateral navigation menu
        if self.revealViewController() != nil {
             menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
            //menuButton.target = self.revealViewController()
            //menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
    }*/
    
    @IBOutlet weak var leyendasTabView: UIView!
    @IBOutlet weak var coleccionesTabView: UIView!
    @IBOutlet weak var bibliotecaTabView: UIView!
    
    @IBAction func didPressTab(_ sender: UIButton) {
        // Tutorial from https://github.com/codepath/ios_guides/wiki/Creating-a-Custom-Tab-Bar
        let previousIndex = selectedIndex
        selectedIndex = sender.tag
        buttons[previousIndex].isSelected = false
        let previousVC = viewControllers[previousIndex]
        
        // Remove the previous ViewController
        previousVC.willMove(toParentViewController: nil)
        previousVC.view.removeFromSuperview()
        previousVC.removeFromParentViewController()
        previousVC.dismiss(animated: false, completion: nil)
        
        // Add the New ViewController and Set Button State
        sender.isSelected = true
        let vc = viewControllers[selectedIndex]
        addChildViewController(vc)
        vc.view.frame = MainContainer.bounds
        MainContainer.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
        
        restoreBottonNavColors()
        
        if(selectedIndex == 0) {
            //self.navigationItem.leftBarButtonItem = nil
            self.backBtnView.isHidden = true
        } else {
            //self.navigationItem.leftBarButtonItem = self.backBtn
            self.backBtnView.isHidden = false
            
            let backColor = UIColor( red: CGFloat(0/255.0), green: CGFloat(134/255.0), blue: CGFloat(218/255.0), alpha: CGFloat(1.0))
            switch (selectedIndex) {
            case 1:
               leyendasTabView.backgroundColor = backColor
                break
            case 2:
               coleccionesTabView.backgroundColor = backColor
                break
            case 3:
                bibliotecaTabView.backgroundColor = backColor
                break
            default:
                break
            }
        }
    }
    
    func restoreBottonNavColors() {
        leyendasTabView.backgroundColor = UIColor.clear
        coleccionesTabView.backgroundColor = UIColor.clear
        bibliotecaTabView.backgroundColor = UIColor.clear
    }
    
    //@IBOutlet weak var menuButton: UIBarButtonItem!
    //@IBOutlet weak var backNavBtn: UIBarButtonItem!
    @IBOutlet weak var GrecaAzul: UIView!
    @IBOutlet var buttons: [UIButton]!
    @IBOutlet weak var MainContainer: UIView!
    @IBOutlet weak var backBtnView: UIView!
    @IBOutlet weak var menuButton: UIButton!
    
    var backBtn: UIBarButtonItem! // Stores a copy of backNavBtn to restore it when need it
    var homeViewController: UIViewController!
    var leyendasViewController: UIViewController!
    var coleccionesViewController: UIViewController!
    var bibliotecaViewController: UIViewController!
    var viewControllers: [UIViewController]! // Array to hold all viewControllers
    var selectedIndex: Int = 0 // variable to keep track of the tab button that is selected
    let defaults = UserDefaults.standard
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setPatterns()
      
      // Delete all previous data from database on books after reinstall
      let userDefaults = UserDefaults.standard
      if userDefaults.bool(forKey: "hasRunBefore") == false {
         // Remove Keychain items here
         
         // Update the flag indicator
         userDefaults.set(true, forKey: "hasRunBefore")
         userDefaults.synchronize() // Forces the app to update UserDefaults
         
         let realm = try! Realm()
         try! realm.write {
            realm.deleteAll()
         }
      }
        
        // NavBackBtn save
        /*let button = UIButton(type: .system)
        button.setImage(UIImage(named: "rsz_backbtn"), for: .normal)
        button.setTitle("Atrás", for: .normal)
        button.addTarget(self, action: #selector(backNavBtnPressed(_:)), for: .touchUpInside)
        button.sizeToFit()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)
        backBtn = UIBarButtonItem(customView: button)*/
        
        // Tap and pan gesture recognition for SWRVC
        if self.revealViewController() != nil {
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
            self.view.addGestureRecognizer(self.revealViewController().tapGestureRecognizer())
        }
        
        // Lateral navigation menu
        if self.revealViewController() != nil {
            menuButton.addTarget(self.revealViewController(), action: #selector(SWRevealViewController.rightRevealToggle(_:)), for: .touchUpInside)
            //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        // instantiate each ViewController by referencing storyboard
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        homeViewController = storyboard.instantiateViewController(withIdentifier: "homeId")
        leyendasViewController = storyboard.instantiateViewController(withIdentifier: "leyendasId")
        coleccionesViewController = storyboard.instantiateViewController(withIdentifier: "coleccionesId")
        bibliotecaViewController = storyboard.instantiateViewController(withIdentifier: "bibliotecaId")
        
        // Add each ViewController to your viewControllers array
        viewControllers = [homeViewController, leyendasViewController, coleccionesViewController, bibliotecaViewController]
        
        // Keep at the bottom
        buttons[selectedIndex].isSelected = true
        didPressTab(buttons[selectedIndex])
        
        // Copy epub_reader folders from bundle to documents directory
        copyFolders()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // Functions
    func setPatterns() {
        // Blue pattern
        UIGraphicsBeginImageContext(self.GrecaAzul.frame.size)
        UIImage(named: "barra_azul_small.png")?.drawAsPattern(in: self.GrecaAzul.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.GrecaAzul.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    func copyFolders() {
        let filemgr = FileManager.default
        filemgr.delegate = self
        let dirPaths = filemgr.urls(for: .documentDirectory, in: .userDomainMask)
        let docsURL = dirPaths[0]
        
        let folderPath = Bundle.main.resourceURL!.appendingPathComponent("epub_reader").path
        let docsFolder = docsURL.appendingPathComponent("epub_reader").path
        copyFiles(pathFromBundle: folderPath, pathDestDocs: docsFolder)
        
        // docsURL	URL	"file:///Users/clickaboom/Library/Developer/CoreSimulator/Devices/8886C2BC-F76B-4129-BADE-DF7C9FE4D670/data/Containers/Data/Application/"
    }
    
    func copyFiles(pathFromBundle : String, pathDestDocs: String) {
        let fileManagerIs = FileManager.default
        fileManagerIs.delegate = self
        
        do {
            let filelist = try fileManagerIs.contentsOfDirectory(atPath: pathFromBundle)
            try? fileManagerIs.copyItem(atPath: pathFromBundle, toPath: pathDestDocs)
            
            for filename in filelist {
                try? fileManagerIs.copyItem(atPath: "\(pathFromBundle)/\(filename)", toPath: "\(pathDestDocs)/\(filename)")
            }
        } catch {
            print("\nError\n")
        }
    }

}
