//
//  LeyendasViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 7/3/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import Nuke

class LeyendasViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var categoriesCollection: UICollectionView!
    @IBOutlet weak var leyendasCollection: UICollectionView!
    @IBOutlet weak var subHeaderView: UIView!
    var categoriasItems:[[String: Any]] = []
    var leyendasItems:[[String: Any]] = []
    var searchText: String = "" {didSet{
        let escapedString = searchText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        loadLeyendas(url: ApiConfig.searchLegends, params: "?q=" + escapedString!)
        }}
    
    @IBAction func mapBtnPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let main = storyboard.instantiateViewController(withIdentifier: "mapsId") as! MapaViewController
        self.present(main, animated: true, completion: nil)    }
    
    // Order buttons
    @IBAction func orderBtnPressed(_ sender: UIButton) {
        restoreOrderColors()
        var params:String = ""
        
        switch sender.tag {
        case 1:
            params = "?order=nuevas"
            break
        case 2:
            params = "?order=populares"
            break
        case 3:
            params = "?order=temadelmes"
            break
        case 4:
            params = ""
            break
        default:
            params = ""
        }
        
        //print(params)
    
        // Set new color of pressed order button
        if let label = self.view.viewWithTag(sender.tag) as? UILabel {
            label.backgroundColor = UIColor( red: CGFloat(55/255.0), green: CGFloat(86/255.0), blue: CGFloat(188/255.0), alpha: CGFloat(1.0))
        }

        loadLeyendas(url:ApiConfig.legends, params:params)
    }
    
    func restoreOrderColors() {
        for var tag in 1 ... 4 {
            if let label = self.view.viewWithTag(tag) as? UILabel {
                label.backgroundColor = UIColor( red: CGFloat(102/255.0), green: CGFloat(132/255.0), blue: CGFloat(226/255.0), alpha: CGFloat(1.0))
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPatterns()
        
        self.categoriesCollection.register(UINib(nibName: "CategoriesViewCell", bundle: nil), forCellWithReuseIdentifier: "categoriasCell")
        self.leyendasCollection.register(UINib(nibName: "BookViewCell", bundle: nil), forCellWithReuseIdentifier: "leyendasCell")
      /*if let flowLayout = leyendasCollection.collectionViewLayout as? UICollectionViewFlowLayout {
         flowLayout.estimatedItemSize = CGSize(width: 1, height:1)
      }*/
      
        self.categoriesCollection.backgroundColor = UIColor( red: CGFloat(215/255.0), green: CGFloat(103/255.0), blue: CGFloat(161/255.0), alpha: CGFloat(1.0))
        
        loadCategorias()
        loadLeyendas(url:ApiConfig.legends, params:"")

    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
        UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.subHeaderView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadCategorias() {
        Alamofire.request(ApiConfig.baseUrl + ApiConfig.legendsCategories, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                //print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                        for itemSet in data {
                            if let itemSet = itemSet as? [[String: Any]] {
                                for item in itemSet {
                                    if let item = item as? [String: Any] {
                                        weakself?.categoriasItems.append(item)
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async() {
                            weakself?.categoriesCollection.reloadData()
                        }
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }

    func loadLeyendas(url:String, params: String) {
        leyendasItems.removeAll()
        let url = ApiConfig.baseUrl + url + params
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                //print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                        for legendsSet in data {
                            if let legendsSet = legendsSet as? [[String: Any]] {
                                for legend in legendsSet {
                                    if let legend = legend as? [String: Any] {
                                        weakself?.leyendasItems.append(legend)
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async() {
                            weakself?.leyendasCollection.reloadData()
                        }
                    }
                }
                
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destinationVC = segue.destination as? SearchViewController {
            destinationVC.callerVC = self
        }
    } 
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoriesCollection {
            return categoriasItems.count
        } else if collectionView == leyendasCollection {
            return leyendasItems.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoriesCollection {
            let element:[String:Any] = categoriasItems[indexPath.row]
            //print(element)
            if let categoryId = element["id"] as? String {
                let cell = collectionView.cellForItem(at: indexPath)
                cell?.backgroundColor = UIColor( red: CGFloat(198/255.0), green: CGFloat(82/255.0), blue: CGFloat(142/255.0), alpha: CGFloat(1.0))

                loadLeyendas(url: ApiConfig.searchLegends, params: "?categoria=" + categoryId);
            }
        } else if collectionView == leyendasCollection {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "detallesId") as! BookDetailsViewController
            vc.mColType = "leyendas"
            vc.mItemId = leyendasItems[indexPath.row]["id"] as! String
         vc.mEpubRef = leyendasItems[indexPath.row]["epub"] as! String
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == categoriesCollection {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.backgroundColor = UIColor( red: CGFloat(215/255.0), green: CGFloat(103/255.0), blue: CGFloat(161/255.0), alpha: CGFloat(1.0))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoriesCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriasCell", for: indexPath) as! CategoriesViewCell
            
            cell.categoryImg.image = nil
            
            if(categoriasItems.count > 0){
                let element:[String:Any] = categoriasItems[indexPath.row]
                //print(element)
                if let imagen = element["icono"] as? String {
                    //print(imagen)
                    let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.catImgPath + imagen)!
                    Nuke.loadImage(with: url, into: cell.categoryImg)
                }
                cell.categoryTitle.text = element["nombre"] as? String
            
                
                self.view.layoutIfNeeded()
                
            }
            
            return cell
        } else if collectionView == leyendasCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "leyendasCell", for: indexPath) as! LeyendasViewCell
            
            cell.bookImg.image = nil
            
            if(leyendasItems.count > 0){
                let element:[String:Any] = leyendasItems[indexPath.row]
                //print(element)
                if let imagen = element["imagen"] as? String {
                    //print(image)
                    let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + imagen)!
                    Nuke.loadImage(with: url, into: cell.bookImg)
                }
                cell.title.text = element["titulo"] as? String
                // Obtener autor
                let autores:[[String: Any]] = (element["autores"] as? [[String : Any]])!
                let autor:[String:Any] = autores[0]
                cell.subtitle.text = autor["autor"] as? String
                
                self.view.layoutIfNeeded()

            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "leyendasCell", for: indexPath) as! LeyendasViewCell
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      let height = self.view.frame.size.height
      let width = self.view.frame.size.width
      if collectionView == categoriesCollection {
         switch UIDevice.current.userInterfaceIdiom {
         case .phone:
            // It's an iPhone
            return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
         case .pad:
            // It's an iPad
            return CGSize(width: width*0.15, height: width*0.15)
         case .unspecified:
            // Uh, oh! What could it be?
            return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
         default:
            // default
            return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
         }
        } else if collectionView == leyendasCollection {
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
               // It's an iPhone
               return CGSize(width: width/3.6, height: height*0.25)
            case .pad:
               // It's an iPad
               return CGSize(width: height*0.2, height: height*0.25)
            case .unspecified:
               // Uh, oh! What could it be?
               return CGSize(width: height*3.6, height: height*0.25)
            default:
               // default
               return CGSize(width: height*3.6, height: height*0.25)
            }
        } else {
            return CGSize(width: self.view.frame.width, height: collectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == categoriesCollection {
            let cellsAcross: CGFloat = 4
            let spacesAcross: CGFloat = 3
            var spaceAdjust: CGFloat = 0
            if categoriasItems.count > 4 {
                spaceAdjust = 10
            }
            let spaceBetweenCells: CGFloat = (collectionView.bounds.width - (collectionView.bounds.height * cellsAcross)) / spacesAcross - spaceAdjust
            return spaceBetweenCells
        } else if collectionView == leyendasCollection {
            return 0
        } else {
            return 0
        }
    }

}
