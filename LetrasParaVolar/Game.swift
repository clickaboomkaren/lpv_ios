//
//  Game.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/30/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class Game {
    var id = ""
    var title = ""
    var subtitle = ""
    var imgResource:UIImage!
    var gameType = ""
    var btnColor:UIColor!
}
