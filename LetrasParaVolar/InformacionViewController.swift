//
//  InformacionViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/21/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import Nuke

class InformacionViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var subHeaderView: UIView!
    @IBOutlet weak var mainContentScrollView: UIScrollView!
    @IBOutlet weak var infoCollection: UICollectionView!
    
    var infoItems:[[String: Any]] = []
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    // Order buttons
    @IBAction func orderBtnPressed(_ sender: UIButton) {
        restoreOrderColors()
        var params:String = ""
        
        switch sender.tag {
        case 1:
            mainContentScrollView.isHidden = false
            infoCollection.isHidden = true
            break
        case 2:
            mainContentScrollView.isHidden = true
            infoCollection.isHidden = false
            break
        default:
            mainContentScrollView.isHidden = false
            infoCollection.isHidden = true
        }
        
        print(params)
        
        // Set new color of pressed order button
        if let label = self.view.viewWithTag(sender.tag) as? UILabel {
            label.backgroundColor = UIColor( red: CGFloat(55/255.0), green: CGFloat(86/255.0), blue: CGFloat(188/255.0), alpha: CGFloat(1.0))
        }
        
    }
    
    func restoreOrderColors() {
        for tag in 1 ... 2 {
            if let label = self.view.viewWithTag(tag) as? UILabel {
                label.backgroundColor = UIColor( red: CGFloat(102/255.0), green: CGFloat(132/255.0), blue: CGFloat(226/255.0), alpha: CGFloat(1.0))
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setPatterns()
        
        self.infoCollection.register(UINib(nibName: "InformationViewCell", bundle: nil), forCellWithReuseIdentifier: "infoCell")
        if let flowLayout = infoCollection.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height:1)
        }
        
        loadInternationalization()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
        UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.subHeaderView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    func loadInternationalization() {
        infoItems.removeAll()
        
        Alamofire.request(ApiConfig.baseUrl + ApiConfig.internacionalization, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                        for itemsSet in data {
                            if let itemsSet = itemsSet as? [String: Any] {
                                //for item in itemsSet {
                                   // if let item = item as? [String: Any] {
                                        weakself?.infoItems.append(itemsSet)
                                    //}
                               // }
                            }
                        }
                        
                        DispatchQueue.main.async() {
                            weakself?.infoCollection.reloadData()
                        }
                    }
                }
                
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == infoCollection {
            return infoItems.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == infoCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "infoCell", for: indexPath) as! InformationViewCell
            
            cell.infoImage.image = nil
            
            if(infoItems.count > 0){
                let element:[String:Any] = infoItems[indexPath.row]
                print(element)
                if let imagen = element["imagen"] as? String {
                    //print(image)
                    let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.juegosImg + imagen)!
                    Nuke.loadImage(with: url, into: cell.infoImage)
                }
                cell.titleLabel.text = element["titulo"] as? String
                // Obtener autor
                //let autores:[[String: Any]] = (element["autores"] as? [[String : Any]])!
                //let autor:[String:Any] = autores[0]
                let contentString = (element["contenido"] as? String)?.replacingOccurrences(of: "background-color: rgb(255, 255, 255);", with: "")
                do {
                    let attrStr = try NSAttributedString(
                        data: (contentString!.data(using: String.Encoding.unicode, allowLossyConversion: true)!),
                        options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                        documentAttributes: nil)
                    cell.descriptionLabel.attributedText = attrStr
                } catch let error {
                    
                }
                
                self.view.layoutIfNeeded()
                
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "infoCell", for: indexPath) as! InformationViewCell
            return cell
        }
    }
    
    /*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == infoCollection {
            return CGSize(width: collectionView.frame.size.width, height: 328.0)
        } else {
            return CGSize(width: self.view.frame.width, height: collectionView.frame.size.height)
        }
    }*/
    
    /*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == infoCollection {
            return 10
        } else {
            return 0
        }
    }*/


}
