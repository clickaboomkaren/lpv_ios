//
//  HomeViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 7/3/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Nuke
import Alamofire
import RealmSwift

class HomeViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
   
   @IBOutlet weak var menuButton: UIBarButtonItem!
   @IBOutlet weak var subHeaderView: UIView!
   @IBOutlet weak var bannerCollection: UICollectionView!
   @IBOutlet weak var leyendasCollection: UICollectionView!
   @IBOutlet weak var coleccionesCollection: UICollectionView!
   @IBOutlet weak var ninioBarcoImg: UIImageView!
   @IBOutlet weak var bannerPageDots: UIPageControl!
   
   @IBOutlet weak var leyPrevImg: UIImageView!
   @IBOutlet weak var leyNextImg: UIImageView!
   @IBOutlet weak var colPrevImg: UIImageView!
   @IBOutlet weak var colNextImg: UIImageView!
    @IBOutlet weak var bannerCollectionHeight: NSLayoutConstraint!
    
   var currentIndex:Int = 1
   var bannerItems:[[String: Any]] = []
   var leyendasItems:[Colecciones] = []
   var coleccionesItems:[Colecciones] = []
   var barquitoFrameWidth:CGFloat = 0.0
   var mBooksLoaded = 0
   var mFirstLegendsRender = true
   var mFirstCollectionRender = true
   let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
   
   @IBAction func arrowsNavOnClick(_ sender: UIButton) {
      var scrollPos = 0
      switch (sender.tag) {
      case 2:
         // leyendas next
         let totalVisibleCount = leyendasCollection.indexPathsForVisibleItems.count
         let lastVisibleIndex = leyendasCollection.indexPathsForVisibleItems.sorted()[totalVisibleCount - 1][1]
         scrollPos = lastVisibleIndex + 2
         if scrollPos >= leyendasItems.count {
            scrollPos = leyendasItems.count - 1
         }
         leyendasCollection.scrollToItem(at: IndexPath(row: scrollPos, section: 0), at: .right, animated: true)
         break
      case 1:
         // leyendas prev
         let totalVisibleCount = leyendasCollection.indexPathsForVisibleItems.count
         let lastVisibleIndex = leyendasCollection.indexPathsForVisibleItems.sorted()[totalVisibleCount - 1][1]
         scrollPos = lastVisibleIndex - 4
         if scrollPos < 0 {
            scrollPos = 0
         }
         leyendasCollection.scrollToItem(at: IndexPath(row: scrollPos, section: 0), at: .left, animated: true)
         break
      case 4:
         // colecciones next
         let totalVisibleCount = coleccionesCollection.indexPathsForVisibleItems.count
         let lastVisibleIndex = coleccionesCollection.indexPathsForVisibleItems.sorted()[totalVisibleCount - 1][1]
         scrollPos = lastVisibleIndex + 2
         if scrollPos >= coleccionesItems.count {
            scrollPos = coleccionesItems.count - 1
         }
         coleccionesCollection.scrollToItem(at: IndexPath(row: scrollPos, section: 0), at: .right, animated: true)
         break
      case 3:
         // colecciones prev
         let totalVisibleCount = coleccionesCollection.indexPathsForVisibleItems.count
         let lastVisibleIndex = coleccionesCollection.indexPathsForVisibleItems.sorted()[totalVisibleCount - 1][1]
         scrollPos = lastVisibleIndex - 4
         if scrollPos < 0 {
            scrollPos = 0
         }
         coleccionesCollection.scrollToItem(at: IndexPath(row: scrollPos, section: 0), at: .left, animated: true)
         break
      default:
         print("default triggered")
         break
      }
   }
   
   
   func setScrollArrowsState(list: [Colecciones], nextBtn: UIButton, prevBtn: UIButton, type: String) {
      let totalVisibleCount = coleccionesCollection.indexPathsForVisibleItems.count
      let firstPos = coleccionesCollection.indexPathsForVisibleItems.sorted()[0][1]
      let lastPos = coleccionesCollection.indexPathsForVisibleItems.sorted()[totalVisibleCount - 1][1]
      let size = list.count
      var nextVal =  false
      var prevVal = false
      
      if firstPos > 0 && lastPos < size-1 {
         nextVal = false
         prevVal = false
      } else if lastPos == size-1 {
         // NextPressed
         nextVal = true
         prevVal = false
      } else if(firstPos == 0) {
         // PrevPressed
         nextVal = false
         prevVal = true
      }
      
      nextBtn.isHidden = nextVal
      prevBtn.isHidden = prevVal
      if type == "leyenda" {
         leyNextImg.isHidden = nextVal
         leyPrevImg.isHidden = prevVal
      } else {
         colNextImg.isHidden = nextVal
         colPrevImg.isHidden = prevVal
      }
   }
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      self.automaticallyAdjustsScrollViewInsets = false
      
      activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
      activityIndicator.color = UIColor.lightGray
      view.addSubview(activityIndicator)
      
      setPatterns()
      if self.revealViewController() != nil {
         menuButton.target = self.revealViewController()
         menuButton.action = #selector(SWRevealViewController.rightRevealToggle(_:))
         //self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
      }
      
      self.bannerCollection.register(UINib(nibName: "BannerViewCell", bundle: nil), forCellWithReuseIdentifier: "bannerCell")
      self.leyendasCollection.register(UINib(nibName: "BookViewCell", bundle: nil), forCellWithReuseIdentifier: "leyendasCell")
      self.coleccionesCollection.register(UINib(nibName: "BookViewCell", bundle: nil), forCellWithReuseIdentifier: "coleccionesCell")
      
      loadBanners()
      
      barquitoFrameWidth = self.view.frame.width
      DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
         // do stuff 5 seconds later
         self.animation(viewAnimation: self.ninioBarcoImg)
      }
   }
   
   override func viewDidAppear(_ animated: Bool) {
      mFirstLegendsRender = true
      mFirstCollectionRender = true
      getLocalEpubs()
      
      setOrientationBasedUI()
   }
   
   func setOrientationBasedUI() {
      let height = self.view.frame.size.height
      let width = self.view.frame.size.width
      switch UIDevice.current.orientation{
      case .portrait:
         bannerCollectionHeight.constant = width*0.3
         //bannerCollection.frame = CGRect(x:bannerCollection.frame.origin.x, y:bannerCollection.frame.origin.y, width:width, height:width*0.3)
      case .portraitUpsideDown:
         bannerCollectionHeight.constant = width*0.3
         //bannerCollection.frame = CGRect(x:bannerCollection.frame.origin.x, y: bannerCollection.frame.origin.y, width:width, height:width*0.3)
      case .landscapeLeft:
         bannerCollectionHeight.constant = height/0.25
         //bannerCollection.frame = CGRect(x:bannerCollection.frame.origin.x, y: bannerCollection.frame.origin.y, width:width, height:height*0.25)
      case .landscapeRight:
         bannerCollectionHeight.constant = height/0.25
         //bannerCollection.frame = CGRect(x:bannerCollection.frame.origin.x, y: bannerCollection.frame.origin.y, width:width, height:height*0.25)
      default:
         bannerCollectionHeight.constant = width/0.3
         //bannerCollection.frame = CGRect(x:bannerCollection.frame.origin.x, y: bannerCollection.frame.origin.y, width:width, height:width*0.3)
      }
      bannerCollection.collectionViewLayout.invalidateLayout()
      self.view.layoutIfNeeded()
   }
   
   override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
      super.viewWillTransition(to: size, with: coordinator)
      if let layout = bannerCollection?.collectionViewLayout as? UICollectionViewFlowLayout{
         layout.itemSize = CGSize(width: 200, height: 200)
      }
   }
   
   func getLocalEpubs() {
      leyendasItems.removeAll()
      coleccionesItems.removeAll()
      
      // Get the default Realm
      //let realm = try! Realm()
      
      do {
         let realm = try Realm()
         let objs = realm.objects(Colecciones.self)
         for item in objs {
            print(item.titulo)
            print(item.bookType)
            print("descargado: \(item.descargado)")
            print("-----")
         }
         
         // Query Realm for all downloaded books
         var itemsAr = realm.objects(Colecciones.self).filter("descargado = true AND bookType = 'leyendas'")
         if !itemsAr.isEmpty {
            leyendasItems.append(contentsOf: itemsAr)
         }
         
         itemsAr = realm.objects(Colecciones.self).filter("descargado = true AND bookType = 'colecciones'")
         //colecciones
         if !itemsAr.isEmpty {
            coleccionesItems.append(contentsOf: itemsAr)
         }
         
      } catch _ {
         // ...
      }
      
      leyendasCollection.reloadData()
      coleccionesCollection.reloadData()
      
      loadLeyendas()
      loadColecciones()
   }
   
   override func viewDidLayoutSubviews() {
      super.viewDidLayoutSubviews()
      bannerCollection.collectionViewLayout.invalidateLayout()
      leyendasCollection.collectionViewLayout.invalidateLayout()
      coleccionesCollection.collectionViewLayout.invalidateLayout()
   }
   
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }
   
   func setPatterns() {
      // Top pattern
      UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
      UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
      
      if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
         UIGraphicsEndImageContext()
         self.subHeaderView.backgroundColor = UIColor(patternImage: image)
      }else{
         UIGraphicsEndImageContext()
         debugPrint("Image not available")
      }
   }
   
   private func animation(viewAnimation: UIView) {
      UIView.animate(withDuration: 5, delay: 0, options: [.curveEaseInOut], animations: {
         viewAnimation.frame.origin.x = +self.barquitoFrameWidth - viewAnimation.frame.width
      }) { (_) in
         UIView.animate(withDuration: 5, delay: 0, options: [.curveEaseInOut], animations: {
            viewAnimation.frame.origin.x -= self.barquitoFrameWidth - viewAnimation.frame.width
         }) { (_) in
            self.animation(viewAnimation: viewAnimation)
         }
         
      }
   }
   
   func loadBanners() {
      Alamofire.request(ApiConfig.baseUrl + ApiConfig.banners, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
         
         switch(response.result) {
         case .success(_):
            //print(response.result.value!)
            
            if let objJson = response.result.value as! [String: Any]? {
               let data = objJson["data"] as! [[String: Any]]?
               self.bannerItems = data!
               
               DispatchQueue.main.async() {
                  self.bannerPageDots.numberOfPages = self.bannerItems.count
                  //self.bannerPageDots.currentPage = 0
                  self.bannerCollection.reloadData()
               }
            }
            break
            
         case .failure(_):
            print(response.result.error)
            break
            
         }
      }
   }
   
   func loadLeyendas() {
      Alamofire.request(ApiConfig.baseUrl + ApiConfig.legendsDefaults, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
         
         switch(response.result) {
         case .success(_):
            //print(response.result.value!)
            if let objJson = response.result.value as! [String: Any]? {
               if let data = objJson["data"] as? NSArray {
                  for bookData in data {
                     if let bookData = bookData as? [String: Any] {
                        let mEpubItem = Colecciones()
                        
                        if let epub = bookData["epub"] as? String {
                           mEpubItem.epub = epub
                        }
                        
                        // Check if book has been previously shown as it is stored in downloads
                        if !self.leyendasItems.contains(where: { $0.epub == mEpubItem.epub }) {
                           
                           // Obtener imagenes del libro
                           mEpubItem.imagen = (bookData["imagen"] as? String)!
                           
                           let image = Imagen()
                           image.imagen = mEpubItem.imagen
                           mEpubItem.imagenes.append(image)
                           
                           // Titulo
                           mEpubItem.titulo = (bookData["titulo"] as? String)!
                           
                           // Obtener categorías
                           /*let catList = List<Categoria>()
                            let categorias:[[String: Any]] = (bookData["categorias"] as? [[String : Any]])!
                            for categoria in categorias {
                            let cat = Categoria()
                            cat.icono = (categoria["icono"] as? String)!
                            cat.categoria = (categoria["categoria"] as? String)!
                            catList.append(cat)
                            }
                            mEpubItem.categorias = catList*/
                           
                           // Obtener autores
                           let autorList = List<Autores>()
                           let autores:[[String: Any]] = (bookData["autores"] as? [[String : Any]])!
                           for autor in autores {
                              let aut = Autores()
                              aut.autor = (autor["autor"] as? String)!
                              autorList.append(aut)
                           }
                           mEpubItem.autores = autorList
                           
                           if let descripcion = bookData["descripcion"] as? String {
                              mEpubItem.descripcion = descripcion
                           }
                           
                           if let editorial = bookData["editorial"] as? String {
                              mEpubItem.editorial = editorial
                           }
                           
                           if let fecha = bookData["fecha"] as? String {
                              mEpubItem.fecha = fecha
                           }
                           if let length = bookData["length"] as? String {
                              mEpubItem.length = length
                           }
                           
                           // Other data
                           if let id = bookData["id"] as? String {
                              mEpubItem.id = id
                           }
                           if let creditos = bookData["creditos"] as? String {
                              mEpubItem.creditos = creditos
                           }
                           if let latitud = bookData["latitud"] as? String {
                              mEpubItem.latitud = latitud
                           }
                           if let longitud = bookData["longitud"] as? String {
                              mEpubItem.longitud = longitud
                           }
                           if let imagen = bookData["imagen"] as? String {
                              mEpubItem.imagen = imagen
                           }
                           if let coleccionesId = bookData["colecciones_id"] as? String {
                              mEpubItem.coleccionesId = coleccionesId
                           }
                           
                           self.leyendasItems.append(mEpubItem)
                        }
                     }
                  }
               }
               DispatchQueue.main.async() {
                  self.leyendasCollection.reloadData()
                  self.downloadDefaultBooks()
               }
            }
            break
            
         case .failure(_):
            print(response.result.error)
            break
            
         }
      }
   }
   
   func loadColecciones() {
      Alamofire.request(ApiConfig.baseUrl + ApiConfig.collectionsDefaults, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { (response:DataResponse<Any>) in
         
         switch(response.result) {
         case .success(_):
            //print(response.result.value!)
            if let objJson = response.result.value as! [String: Any]? {
               if let data = objJson["data"] as? NSArray {
                  for bookData in data {
                     if let bookData = bookData as? [String: Any] {
                        let mEpubItem = Colecciones()
                        
                        if let epub = bookData["epub"] as? String {
                           mEpubItem.epub = epub
                        }
                        
                        // Check if book has been previously shown as it is stored in downloads
                        if !self.coleccionesItems.contains(where: { $0.epub == mEpubItem.epub }) {
                           
                           // Obtener imagenes del libro
                           mEpubItem.imagen = (bookData["imagen"] as? String)!
                           
                           let image = Imagen()
                           image.imagen = mEpubItem.imagen
                           mEpubItem.imagenes.append(image)
                           
                           // Titulo
                           mEpubItem.titulo = (bookData["titulo"] as? String)!
                           
                           // Obtener categorías
                           /*let catList = List<Categoria>()
                            let categorias:[[String: Any]] = (bookData["categorias"] as? [[String : Any]])!
                            for categoria in categorias {
                            let cat = Categoria()
                            cat.icono = (categoria["icono"] as? String)!
                            cat.categoria = (categoria["categoria"] as? String)!
                            catList.append(cat)
                            }
                            mEpubItem.categorias = catList*/
                           
                           // Obtener autores
                           let autorList = List<Autores>()
                           let autores:[[String: Any]] = (bookData["autores"] as? [[String : Any]])!
                           for autor in autores {
                              let aut = Autores()
                              aut.autor = (autor["autor"] as? String)!
                              autorList.append(aut)
                           }
                           mEpubItem.autores = autorList
                           
                           if let descripcion = bookData["descripcion"] as? String {
                              mEpubItem.descripcion = descripcion
                           }
                           
                           if let editorial = bookData["editorial"] as? String {
                              mEpubItem.editorial = editorial
                           }
                           
                           if let fecha = bookData["fecha"] as? String {
                              mEpubItem.fecha = fecha
                           }
                           if let length = bookData["length"] as? String {
                              mEpubItem.length = length
                           }
                           
                           // Other data
                           if let id = bookData["id"] as? String {
                              mEpubItem.id = id
                           }
                           if let creditos = bookData["creditos"] as? String {
                              mEpubItem.creditos = creditos
                           }
                           if let latitud = bookData["latitud"] as? String {
                              mEpubItem.latitud = latitud
                           }
                           if let longitud = bookData["longitud"] as? String {
                              mEpubItem.longitud = longitud
                           }
                           if let imagen = bookData["imagen"] as? String {
                              mEpubItem.imagen = imagen
                           }
                           if let coleccionesId = bookData["colecciones_id"] as? String {
                              mEpubItem.coleccionesId = coleccionesId
                           }
                           
                           self.coleccionesItems.append(mEpubItem)
                        }
                     }
                  }
               }
               DispatchQueue.main.async() {
                  self.coleccionesCollection.reloadData()
                  self.downloadDefaultBooks()
               }
            }
            break
            
         case .failure(_):
            print(response.result.error)
            break
            
         }
      }
   }
   
   // MARK: Checks if both leyendasList and coleccionesList have data already
   func downloadDefaultBooks() {
      mBooksLoaded += 1
      if mBooksLoaded == 2 {
         recursiveDownload()
      }
   }
   
   func recursiveDownload() {
      activityIndicator.startAnimating()
      for item:Colecciones in leyendasItems {
         fileExists(item: item, bookType: "leyendas")
      }
      
      for item:Colecciones in coleccionesItems {
         fileExists(item: item, bookType: "colecciones")
      }
      
      self.activityIndicator.stopAnimating()
   }
   
   func fileExists(item: Colecciones, bookType: String) {
      let mEpubFolder = item.epub.replacingOccurrences(of: ".epub", with: "")
      let mEpubRoute = "epub_reader/epubs/" + mEpubFolder
      
      let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
      let url = NSURL(fileURLWithPath: path)
      let filePath = url.appendingPathComponent(mEpubRoute)?.path
      let fileManager = FileManager.default
      //"url	NSURL	"file:///Users/clickaboom/Library/Developer/CoreSimulator/Devices/8886C2BC-F76B-4129-BADE-DF7C9FE4D670/data/Containers/Data/Application/EBD273DE-D29D-4F32-9251-08FED3744835/Documents/"	0x7d1d8980"
      if !fileManager.fileExists(atPath: filePath!) {
         // Epub doesn't exist yet
         do {
            try fileManager.createDirectory(atPath: filePath!, withIntermediateDirectories: false, attributes: nil)
         } catch {
            print("Error creating epub folder in documents dir: \(error)")
         }
         
         item.favorito = false
         item.bookType = bookType
         downloadEpub(mEpubItem: item, mEpubRoute: mEpubRoute)
      }
   }
   
   func downloadEpub(mEpubItem: Colecciones, mEpubRoute: String) {
      var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      documentsURL.appendPathComponent(mEpubRoute + "/" + mEpubItem.epub)
      
      let destination: DownloadRequest.DownloadFileDestination = { _, _ in
         return (documentsURL, [.removePreviousFile])
      }
      let url = ApiConfig.baseUrl + ApiConfig.epubs + mEpubItem.epub
      Alamofire.download(
         url,
         method: .get,
         parameters: nil,
         encoding: JSONEncoding.default,
         headers: nil,
         to: destination).downloadProgress(closure: { (progress) in
            //progress closure
         }).response(completionHandler: { (DefaultDownloadResponse) in
            print("downloaded")
            
            do {
               let realm = try Realm()
               try? realm.write {
                  mEpubItem.descargado = true
                  mEpubItem.favorito = false
                  realm.add(mEpubItem, update: true)
               }
            } catch let error as NSError {
               // handle error
               print(error)
            }
         })
   }
   
   func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
      if collectionView == bannerCollection {
         return bannerItems.count
         return 50
      } else if collectionView == leyendasCollection {
         return leyendasItems.count
      } else if collectionView == coleccionesCollection {
         return coleccionesItems.count
      } else {
         return 0
      }
   }
   
   func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      if collectionView == bannerCollection {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bannerCell", for: indexPath) as! BannerViewCell
         //cell.image.image = imgArray[indexPath.row]
         
         cell.image.image = nil
         
         if(bannerItems.count > 0){
            //Infinite scroll
            //let element:[String:Any] = bannerItems[indexPath.row % bannerItems.count]
            //print(element)
            let element:[String:Any] = bannerItems[indexPath.row]
            if let imagen = element["imagen"] as! String? {
               //print(image)
               let url = URL.init(string:ApiConfig.baseUrl + "/" + imagen)!
               Nuke.loadImage(with: url, into: cell.image)
               //cell.heightConstraint.constant = (cell.image.frame.height)
               self.view.layoutIfNeeded()
            }
         }
         
         // Cell heigth as image height
         //cell.heightConstraint.constant = collectionView.contentSize.height
         //cell.layoutIfNeeded()
         //bannerCell = cell
         return cell
      } else if collectionView == leyendasCollection {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "leyendasCell", for: indexPath) as! LeyendasViewCell
         
         cell.bookImg.image = nil
         
         if(leyendasItems.count > 0){
            let element:Colecciones = leyendasItems[indexPath.row % leyendasItems.count]
            
            cell.title.numberOfLines = 1
            cell.subtitle.numberOfLines = 1
            
            if element.imagenes.count > 0 {
               let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + element.imagenes[0].imagen)!
               Nuke.loadImage(with: url, into: cell.bookImg)
               
               // Obtener autor
               let autores = element.autores
               let autor:Autores = autores[0]
               cell.title.text = element.titulo as String?
               cell.subtitle.text = autor.autor as String?
            } else {
               let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + element.imagen)!
               Nuke.loadImage(with: url, into: cell.bookImg)
               /*cell.subtitle.numberOfLines = 1
                cell.title.lineBreakMode = NSLineBreakMode.byWordWrapping
                cell.title.text = element.titulo*/
               cell.title.text = element.titulo as String?
               cell.subtitle.text = element.autores[0].autor
            }
            
            self.view.layoutIfNeeded()
         }
         
         return cell
         
      } else if collectionView == coleccionesCollection {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coleccionesCell", for: indexPath) as! LeyendasViewCell
         
         cell.bookImg.image = nil
         
         if(coleccionesItems.count > 0){
            let element:Colecciones = coleccionesItems[indexPath.row % coleccionesItems.count]
            
            cell.title.numberOfLines = 1
            cell.subtitle.numberOfLines = 1
            
            if element.imagenes.count > 0 {
               let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + element.imagenes[0].imagen)!
               Nuke.loadImage(with: url, into: cell.bookImg)
               
               // Obtener autor
               let autores = element.autores
               let autor:Autores = autores[0]
               cell.title.text = element.titulo as String?
               cell.subtitle.text = autor.autor as String?
            } else {
               let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + element.imagen)!
               Nuke.loadImage(with: url, into: cell.bookImg)
               cell.title.text = element.titulo
               cell.subtitle.text = element.autores[0].autor
            }
            
            self.view.layoutIfNeeded()
         }
         
         return cell
         
      } else {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bannerCell", for: indexPath) as! BannerViewCell
         //cell.image.image = imgArray[indexPath.row]
         
         cell.image.image = nil
         
         if(bannerItems.count > 0){
            let element:[String:Any] = bannerItems[indexPath.row % bannerItems.count]
            //print(element)
            if let imagen = element["imagen"] as! String? {
               //print(image)
               let url = URL.init(string:ApiConfig.baseUrl + "/" + imagen)!
               Nuke.loadImage(with: url, into: cell.image)
            //   cell.heightConstraint.constant = (cell.image.frame.height)
               self.view.layoutIfNeeded()
            }
         }
         
         return cell
      }
   }
   
   func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      if collectionView == leyendasCollection {
         let vc = self.storyboard!.instantiateViewController(withIdentifier: "detallesId") as! BookDetailsViewController
         vc.mColType = "leyendas"
         vc.mItemId = leyendasItems[indexPath.row]["id"] as! String
         vc.mEpubRef = leyendasItems[indexPath.row]["epub"] as! String
         self.present(vc, animated: true, completion: nil)
      } else if collectionView == coleccionesCollection {
         let vc = self.storyboard!.instantiateViewController(withIdentifier: "detallesId") as! BookDetailsViewController
         vc.mColType = "colecciones"
         vc.mItemId = coleccionesItems[indexPath.row]["id"] as! String
         vc.mEpubRef = coleccionesItems[indexPath.row]["epub"] as! String
         self.present(vc, animated: true, completion: nil)
      }
   }
   
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      let height = self.view.frame.size.height
      let width = self.view.frame.size.width
      if collectionView == leyendasCollection || collectionView == coleccionesCollection {
         switch UIDevice.current.userInterfaceIdiom {
         case .phone:
            // It's an iPhone
            return CGSize(width: width/3.6, height: height*0.25)
         case .pad:
            // It's an iPad
            return CGSize(width: height*0.2, height: height*0.25)
         case .unspecified:
            // Uh, oh! What could it be?
            return CGSize(width: height*3.6, height: height*0.25)
         default:
            // default
            return CGSize(width: height*3.6, height: height*0.25)
         }
      } else {
         switch UIDevice.current.userInterfaceIdiom {
         case .phone:
            // It's an iPhone
            return CGSize(width: width, height: width*0.3)
         case .pad:
            // It's an iPad
            return CGSize(width: width, height: height*0.25)
         case .unspecified:
            // Uh, oh! What could it be?
            return CGSize(width: width, height: width*0.3)
         default:
            // default
            return CGSize(width: width, height: width*0.3)
         }
      }
   }
   
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      if collectionView == leyendasCollection || collectionView == coleccionesCollection {
         switch UIDevice.current.userInterfaceIdiom {
         case .phone:
            // It's an iPhone
            return self.view.frame.size.width*0.05
         case .pad:
            // It's an iPad
            return self.view.frame.size.width*0.15
         case .unspecified:
            // Uh, oh! What could it be?
            return self.view.frame.size.width*0.05
         default:
            // default
            return self.view.frame.size.width*0.05
         }
      } else {
         return 0
      }
   }
   
   func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
      if collectionView ==  bannerCollection {
         self.bannerPageDots.currentPage = indexPath.row
      } else {
         var size = 0
         if collectionView == leyendasCollection {
            size = leyendasItems.count
         } else {
            size = coleccionesItems.count
         }
         
         var nextVal =  false
         var prevVal = false
         
         if collectionView == leyendasCollection {
            // Check if there are less than 3 items then don't show arrows
            if leyendasItems.count <= 3 {
               nextVal = true
               prevVal = true
            } else {
               if indexPath.row > 0 && indexPath.row < size-1 {
                  // Central Item / Show both arrows
                  nextVal = false
                  prevVal = false
               } else if indexPath.row == size-1 {
                  // NextPressed / Hide next - Show prev
                  nextVal = true
                  prevVal = false
               } else if(indexPath.row == 0) {
                  // PrevPressed / Show next - Hide prev
                  nextVal = false
                  prevVal = true
               }
               
               if mFirstLegendsRender {
                  // If there are more than 3 items and the first one is shown then just show next arrow
                  mFirstLegendsRender = false
                  nextVal = false
                  prevVal = true
               }
            }
         } else if collectionView == coleccionesCollection {
            // Check if there are less than 3 items then don't show arrows
            if coleccionesItems.count <= 3 {
               nextVal = true
               prevVal = true
            } else {
               if indexPath.row > 0 && indexPath.row < size-1 {
                  // Central Item / Show both arrows
                  nextVal = false
                  prevVal = false
               } else if indexPath.row == size-1 {
                  // NextPressed / Hide next - Show prev
                  nextVal = true
                  prevVal = false
               } else if(indexPath.row == 0) {
                  // PrevPressed / Show next - Hide prev
                  nextVal = false
                  prevVal = true
               }
               
               if mFirstCollectionRender {
                  // If there are more than 3 items and the first one is shown then just show next arrow
                  mFirstCollectionRender = false
                  nextVal = false
                  prevVal = true
               }
            }
         }
         
         if collectionView == leyendasCollection {
            leyNextImg.isHidden = nextVal
            leyPrevImg.isHidden = prevVal
         } else {
            colNextImg.isHidden = nextVal
            colPrevImg.isHidden = prevVal
         }
      }
   }
   
   // Called when the cell is displayed
   /*func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
    var nextVal =  false
    var prevVal = false
    
    if collectionView == leyendasCollection {
    if leyendasItems.count > 3 || coleccionesItems.count > 3 {
    nextVal = false
    prevVal = true
    }
    }
    
    if collectionView == leyendasCollection {
    leyNextImg.isHidden = nextVal
    leyPrevImg.isHidden = prevVal
    } else {
    colNextImg.isHidden = nextVal
    colPrevImg.isHidden = prevVal
    }
    }*/
   
   /*func scrollViewDidScroll(_ scrollView: UIScrollView) {
    self.setScrollArrowsState(list: <#T##[Colecciones]#>, nextBtn: <#T##UIButton#>, prevBtn: <#T##UIButton#>, type: <#T##String#>)
    }*/
}
