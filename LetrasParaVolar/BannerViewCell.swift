//
//  BannerViewCell.swift
//  LetrasParaVolar
//
//  Created by Desarrollo on 17/12/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class BannerViewCell: UICollectionViewCell {

    @IBOutlet weak var image: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
