//
//  Pregunta.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 9/3/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class Pregunta {
    var id = ""
    var pregunta = ""
    var status = ""
    var respuestas = [Respuesta]()
}
