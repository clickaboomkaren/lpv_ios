//
//  LeyendasViewCell.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 7/10/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class LeyendasViewCell: UICollectionViewCell {
    
    @IBOutlet var bookImg: UIImageView!
    @IBOutlet var title: UILabel!
    @IBOutlet var subtitle: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        /*self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        widthConstraint.constant = screenWidth - (2 * 12)*/
    }
}
