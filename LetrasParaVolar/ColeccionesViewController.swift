//
//  ColeccionesViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 7/5/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import Nuke

class ColeccionesViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var categoriesCollection: UICollectionView!
    @IBOutlet weak var coleccionesCollection: UICollectionView!
    @IBOutlet weak var subHeaderView: UIView!
    var categoriasItems:[[String: Any]] = []
    var coleccionesItems:[[String: Any]] = []
    var searchText: String = "" {didSet{
        let escapedString = searchText.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        loadColecciones(url: ApiConfig.searchCollections, params: "?q=" + escapedString!)
        }}
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
        UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.subHeaderView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    // Order buttons
    @IBAction func orderBtnPressed(_ sender: UIButton) {
        restoreOrderColors()
        var params:String = ""
        
        switch sender.tag {
        case 1:
            params = "?order=nuevas"
            break
        case 2:
            params = "?order=populares"
            break
        case 3:
            params = "?order=temadelmes"
            break
        case 4:
            params = ""
            break
        default:
            params = ""
        }
        
        print(params)
        
        // Set new color of pressed order button
        if let label = self.view.viewWithTag(sender.tag) as? UILabel {
            label.backgroundColor = UIColor( red: CGFloat(55/255.0), green: CGFloat(86/255.0), blue: CGFloat(188/255.0), alpha: CGFloat(1.0))
        }
        
        loadColecciones(url:ApiConfig.collections, params:params)
    }
    
    func restoreOrderColors() {
        for var tag in 1 ... 4 {
            if let label = self.view.viewWithTag(tag) as? UILabel {
                label.backgroundColor = UIColor( red: CGFloat(102/255.0), green: CGFloat(132/255.0), blue: CGFloat(226/255.0), alpha: CGFloat(1.0))
            }
        }
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPatterns()
        
        self.categoriesCollection.register(UINib(nibName: "CategoriesViewCell", bundle: nil), forCellWithReuseIdentifier: "categoriasCell")
        self.coleccionesCollection.register(UINib(nibName: "BookViewCell", bundle: nil), forCellWithReuseIdentifier: "coleccionesCell")
      /*if let flowLayout = coleccionesCollection.collectionViewLayout as? UICollectionViewFlowLayout {
         flowLayout.estimatedItemSize = CGSize(width: 1, height:1)
      }*/
      
        self.categoriesCollection.backgroundColor = UIColor( red: CGFloat(154/255.0), green: CGFloat(193/255.0), blue: CGFloat(84/255.0), alpha: CGFloat(1.0))
        
        loadCategorias()
        loadColecciones(url:ApiConfig.collections, params:"")
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadCategorias() {
        Alamofire.request(ApiConfig.baseUrl + ApiConfig.collectionsCategories, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                        for itemSet in data {
                            if let itemSet = itemSet as? [[String: Any]] {
                                for item in itemSet {
                                    if let item = item as? [String: Any] {
                                        weakself?.categoriasItems.append(item)
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async() {
                            weakself?.categoriesCollection.reloadData()
                        }
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        if let destinationVC = segue.destination as? SearchViewController {
            destinationVC.callerVC = self
        }
    }
    
    func loadColecciones(url:String, params: String) {
        coleccionesItems.removeAll()
        
        Alamofire.request(ApiConfig.baseUrl + url + params, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                        for collectionSet in data {
                            if let collectionSet = collectionSet as? [[String: Any]] {
                                for collection in collectionSet {
                                    if let collection = collection as? [String: Any] {
                                        weakself?.coleccionesItems.append(collection)
                                    }
                                }
                            }
                        }
                        
                        DispatchQueue.main.async() {
                            weakself?.coleccionesCollection.reloadData()
                        }
                    }
                }
                
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == categoriesCollection {
            return categoriasItems.count
        } else if collectionView == coleccionesCollection {
            return coleccionesItems.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == categoriesCollection {
            let element:[String:Any] = categoriasItems[indexPath.row]
            print(element)
            if let categoryId = element["id"] as? String {
                let cell = collectionView.cellForItem(at: indexPath)
                cell?.backgroundColor = UIColor( red: CGFloat(169/255.0), green: CGFloat(202/255.0), blue: CGFloat(99/255.0), alpha: CGFloat(1.0))
                
                loadColecciones(url: ApiConfig.searchCollections, params: "?categoria=" + categoryId);
            }
        } else if collectionView == coleccionesCollection {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "detallesId") as! BookDetailsViewController
            vc.mColType = "colecciones"
            vc.mItemId = coleccionesItems[indexPath.row]["id"] as! String
         vc.mEpubRef = coleccionesItems[indexPath.row]["epub"] as! String
            self.present(vc, animated: true, completion: nil)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        if collectionView == categoriesCollection {
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.backgroundColor = UIColor( red: CGFloat(154/255.0), green: CGFloat(193/255.0), blue: CGFloat(84/255.0), alpha: CGFloat(1.0))
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == categoriesCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoriasCell", for: indexPath) as! CategoriesViewCell
            
            cell.categoryImg.image = nil
            
            if(categoriasItems.count > 0){
                let element:[String:Any] = categoriasItems[indexPath.row]
                print(element)
                if let imagen = element["icono"] as? String {
                    print(imagen)
                    let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.catImgPath + imagen)!
                    Nuke.loadImage(with: url, into: cell.categoryImg)
                }
                cell.categoryTitle.text = element["nombre"] as? String
                
                
                self.view.layoutIfNeeded()
                
            }
            
            return cell
        } else if collectionView == coleccionesCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coleccionesCell", for: indexPath) as! LeyendasViewCell
            
            cell.bookImg.image = nil
            
            if(coleccionesItems.count > 0){
                let element:[String:Any] = coleccionesItems[indexPath.row]
                print(element)
                if let imagen = element["imagen"] as? String {
                    //print(image)
                    let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + imagen)!
                    Nuke.loadImage(with: url, into: cell.bookImg)
                }
                cell.title.text = element["titulo"] as? String
                // Obtener autor
                let autores:[[String: Any]] = (element["autores"] as? [[String : Any]])!
                let autor:[String:Any] = autores[0]
                cell.subtitle.text = autor["autor"] as? String
                
                self.view.layoutIfNeeded()
                
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coleccionesCell", for: indexPath) as! LeyendasViewCell
            return cell
        }
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
         let height = self.view.frame.size.height
         let width = self.view.frame.size.width
        if collectionView == categoriesCollection {
         switch UIDevice.current.userInterfaceIdiom {
         case .phone:
            // It's an iPhone
            return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
         case .pad:
            // It's an iPad
            return CGSize(width: width*0.15, height: width*0.15)
         case .unspecified:
            // Uh, oh! What could it be?
            return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
         default:
            // default
            return CGSize(width: collectionView.bounds.height, height: collectionView.bounds.height)
         }
        } else if collectionView == coleccionesCollection {
            switch UIDevice.current.userInterfaceIdiom {
            case .phone:
               // It's an iPhone
               return CGSize(width: width/3.6, height: height*0.25)
            case .pad:
               // It's an iPad
               return CGSize(width: height*0.2, height: height*0.25)
            case .unspecified:
               // Uh, oh! What could it be?
               return CGSize(width: height*3.6, height: height*0.25)
            default:
               // default
               return CGSize(width: height*3.6, height: height*0.25)
            }
        } else {
            return CGSize(width: self.view.frame.width, height: collectionView.frame.size.height)
        }    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == categoriesCollection {
            var cellsAcross: CGFloat = 4
            let spacesAcross: CGFloat = 3
            var spaceAdjust: CGFloat = 0
            if categoriasItems.count > 4 {
                spaceAdjust = 10
            }
         
         switch UIDevice.current.userInterfaceIdiom {
         case .phone:
            // It's an iPhone
            cellsAcross = 4
         case .pad:
            // It's an iPad
            if categoriasItems.count > 6 {
               spaceAdjust = 10
               cellsAcross = 6
            } else {
               cellsAcross = CGFloat(categoriasItems.count)
            }
         case .unspecified:
            // Uh, oh! What could it be?
            cellsAcross = 4
         default:
            // default
            cellsAcross = 4
         }
         
            let spaceBetweenCells: CGFloat = (collectionView.bounds.width - (collectionView.bounds.height * cellsAcross)) / spacesAcross - spaceAdjust
            return spaceBetweenCells
        } else if collectionView == coleccionesCollection {
            return 0
        } else {
            return 0
        }
    }
}
