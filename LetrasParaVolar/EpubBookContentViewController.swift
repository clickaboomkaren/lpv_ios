//
//  EpubBookContentViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/16/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import SSZipArchive
import RealmSwift

class EpubBookContentViewController: UIViewController, UIWebViewDelegate {
   
   var mEpubItem = Colecciones()
   var mEpubFolder = ""
   var mEpubRoute = ""
   let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
   
   @IBAction func backBtnPressed(_ sender: UIButton) {
      // Select homeViewController
      navigationController?.popViewController(animated: true)
      dismiss(animated: true, completion: nil)
   }
   
   @IBOutlet weak var webView: UIWebView!
   
   override func viewDidLoad() {
      super.viewDidLoad()
      
      UIApplication.shared.isNetworkActivityIndicatorVisible = true
      
      activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
      activityIndicator.color = UIColor.lightGray
      view.addSubview(activityIndicator)
      activityIndicator.startAnimating()
      
      webView.frame = self.view.frame
      webView.scalesPageToFit = false
      
      mEpubFolder = mEpubItem.epub.replacingOccurrences(of: ".epub", with: "")
      mEpubRoute = "epub_reader/epubs/" + mEpubFolder
      
      fileExists()
   }
   
   override func didReceiveMemoryWarning() {
      super.didReceiveMemoryWarning()
      // Dispose of any resources that can be recreated.
   }
   
   func webViewDidStartLoad(_ webView : UIWebView) {
   }
   
   func webViewDidFinishLoad(_ webView : UIWebView) {
      UIApplication.shared.isNetworkActivityIndicatorVisible = false
      activityIndicator.stopAnimating()
      webView.updateLayoutForContent()
   }
   
   func fileExists() {
      let path = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as String
      let url = NSURL(fileURLWithPath: path)
      let filePath = url.appendingPathComponent(mEpubRoute)?.path
      let fileManager = FileManager.default
      //"url	NSURL	"file:///Users/clickaboom/Library/Developer/CoreSimulator/Devices/8886C2BC-F76B-4129-BADE-DF7C9FE4D670/data/Containers/Data/Application/EBD273DE-D29D-4F32-9251-08FED3744835/Documents/"	0x7d1d8980"
      if fileManager.fileExists(atPath: filePath!) {
         // Epub already downloaded
         loadEpubInWebView()
      } else {
         // Epub doesn't exist yet
         do {
            try fileManager.createDirectory(atPath: filePath!, withIntermediateDirectories: false, attributes: nil)
         } catch {
            print("Error creating epub folder in documents dir: \(error)")
         }
         downloadEpub(mEpubItem: mEpubItem, mEpubRoute: mEpubRoute)
      }
   }
   
   func loadEpubInWebView() {
      // Unzip
      var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      documentsURL.appendPathComponent(mEpubRoute + "/" + mEpubItem.epub)
      var unzipPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      unzipPath.appendPathComponent(mEpubRoute)
      SSZipArchive.unzipFile(atPath: documentsURL.path, toDestination: unzipPath.path)
      
      let fileManager = FileManager.default
      var url = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
      url = url.appendingPathComponent("epub_reader/index.html")
      
      let parameters:[String:AnyObject] = ["book": mEpubFolder as AnyObject]
      //        let parameters:[String:AnyObject] = ["book": "lpv1" as AnyObject]
      let parameterString = parameters.stringFromHttpParameters()
      let requestURL = URL(string:"\(url)?\(parameterString)")!
      //let requestURL = URL(string: "https://realm.io/docs/tutorials/scanner/#overview")!
      
      var request = URLRequest(url: requestURL)
      request.httpMethod = "GET"
      webView.loadRequest(request)
   }
   
   func downloadEpub(mEpubItem: Colecciones, mEpubRoute: String) {
      var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
      documentsURL.appendPathComponent(mEpubRoute + "/" + mEpubItem.epub)
      
      let destination: DownloadRequest.DownloadFileDestination = { _, _ in
         return (documentsURL, [.removePreviousFile])
      }
      let url = ApiConfig.baseUrl + ApiConfig.epubs + mEpubItem.epub
      Alamofire.download(
         url,
         method: .get,
         parameters: nil,
         encoding: JSONEncoding.default,
         headers: nil,
         to: destination).downloadProgress(closure: { (progress) in
            //progress closure
         }).response(completionHandler: { (DefaultDownloadResponse) in
            print("downloaded")
            self.loadEpubInWebView()
            do {
               let realm = try Realm()
               try? realm.write {
                  // Delete related books as they won't be used in local storage
                  // and they cause trouble for rendering downloaded books
                  mEpubItem.librosRelacionados.removeAll()
                  mEpubItem.descargado = true
                  mEpubItem.favorito = false
                  realm.add(mEpubItem, update: true)
               }
            } catch let error as NSError {
               // handle error
               print(error)
            }
         })
   }
   
   /*func downloadEpub() {
    var documentsURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
    documentsURL.appendPathComponent(mEpubRoute + "/" + mEpubItem.epub)
    
    let destination: DownloadRequest.DownloadFileDestination = { _, _ in
    return (documentsURL, [.removePreviousFile])
    }
    let url = ApiConfig.baseUrl + ApiConfig.epubs + mEpubItem.epub
    Alamofire.download(
    url,
    method: .get,
    parameters: nil,
    encoding: JSONEncoding.default,
    headers: nil,
    to: destination).downloadProgress(closure: { (progress) in
    //progress closure
    }).response(completionHandler: { (DefaultDownloadResponse) in
    print("downloaded")
    
    self.loadEpubInWebView()
    
    // Create or update the object
    do {
    let realm = try Realm()
    let itemsAr = realm.object(ofType: Colecciones.self, forPrimaryKey: self.mEpubItem.epub)
    if itemsAr != nil {
    print("epub repetido \(itemsAr?.epub)")
    print("epub repetido \(itemsAr?.titulo)")
    }
    try? realm.write {
    self.mEpubItem.descargado = true
    self.mEpubItem.favorito = false
    realm.add(self.mEpubItem, update: true)
    }
    } catch let error as NSError {
    // handle error
    print(error)
    }
    
    /*DispatchQueue(label: "background").async {
    do {
    let realm = try Realm()
    let itemsAr = realm.object(ofType: Colecciones.self, forPrimaryKey: self.mEpubItem.epub)
    try! realm.write {
    if itemsAr == nil {
    self.mEpubItem.descargado = true
    self.mEpubItem.favorito = false
    realm.add(self.mEpubItem)
    } else {
    itemsAr?.descargado = true
    itemsAr?.favorito = false
    }
    }
    } catch let error as NSError {
    // handle error
    print(error)
    }
    }*/
    
    // Get the default Realm
    /*let realm = try! Realm()
    let itemsAr = realm.object(ofType: Colecciones.self, forPrimaryKey: self.mEpubItem.epub)
    try! realm.write {
    if itemsAr != nil {
    realm.delete(itemsAr!)
    }
    realm.add(self.mEpubItem)
    }*/
    })
    }*/
}

extension String {
   
   /// Percent escapes values to be added to a URL query as specified in RFC 3986
   ///
   /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "~".
   ///
   /// http://www.ietf.org/rfc/rfc3986.txt
   ///
   /// :returns: Returns percent-escaped string.
   
   func addingPercentEncodingForURLQueryValue() -> String? {
      let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~")
      
      return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
   }
   
}

extension Dictionary {
   
   /// Build string representation of HTTP parameter dictionary of keys and objects
   ///
   /// This percent escapes in compliance with RFC 3986
   ///
   /// http://www.ietf.org/rfc/rfc3986.txt
   ///
   /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped
   
   func stringFromHttpParameters() -> String {
      let parameterArray = self.map { (key, value) -> String in
         let percentEscapedKey = (key as! String).addingPercentEncodingForURLQueryValue()!
         let percentEscapedValue = (value as! String).addingPercentEncodingForURLQueryValue()!
         return "\(percentEscapedKey)=\(percentEscapedValue)"
      }
      
      return parameterArray.joined(separator: "&")
   }
   
}

extension UIWebView {
   
   func updateLayoutForContent() {
      let javascript = "document.querySelector('meta[name=viewport]').setAttribute('content', 'width=\(self.bounds.size.width);', false); "
      self.stringByEvaluatingJavaScript(from: javascript)
   }
   
}

