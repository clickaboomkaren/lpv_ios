//
//  NoticiasViewCell.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/24/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class NoticiasViewCell: UICollectionViewCell {

    @IBOutlet weak var notiImg: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var readMoreContainer: UIView!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgWidthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.readMoreContainer.backgroundColor = .clear
        self.readMoreContainer.layer.cornerRadius = 5
        self.readMoreContainer.layer.borderWidth = 1
        let myColor: UIColor = UIColor( red: CGFloat(86/255.0), green: CGFloat(153/255.0), blue: CGFloat(183/255.0), alpha: CGFloat(1.0))
        self.readMoreContainer.layer.borderColor = myColor.cgColor
        
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        widthConstraint.constant = screenWidth - (2 * 12)
    }

}
