//
//  RalmDbHelper.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/18/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import RealmSwift

final class Colecciones: Object {
    dynamic var id = ""
    dynamic var titulo = ""
    dynamic var fecha = ""
    dynamic var epub = ""
    dynamic var descripcion = ""
    dynamic var editorial = ""
    dynamic var length = ""
    dynamic var creditos = ""
    var autores = List<Autores>()
    dynamic var latitud = ""
    dynamic var longitud = ""
    dynamic var imagen = ""
    var imagenes = List<Imagen>()
    dynamic var coleccionesId = ""
    var categorias = List<Categoria>()
    var etiquetas = List<Etiqueta>()
    var librosRelacionados = List<Colecciones>()
    
    dynamic var favorito = false
    dynamic var descargado = false
    dynamic var bookType = ""
    
    override static func primaryKey() -> String? {
        return "epub"
    }
}
