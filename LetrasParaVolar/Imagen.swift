//
//  Imagen.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/18/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import RealmSwift

final class Imagen: Object {
    dynamic var imagen = ""
    dynamic var size = ""
}
