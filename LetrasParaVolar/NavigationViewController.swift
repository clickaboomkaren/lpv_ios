//
//  NavigationViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 7/2/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class NavigationViewController: UIViewController {
    
    @IBAction func InfoClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let main = storyboard.instantiateViewController(withIdentifier: "informacionId") as! InformacionViewController
        //let nav = UINavigationController(rootViewController: main)
        
        self.present(main, animated: true, completion: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func NoticiasClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let main = storyboard.instantiateViewController(withIdentifier: "noticiasId") as! NoticiasViewController
        self.present(main, animated: true, completion: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func JuegosClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let main = storyboard.instantiateViewController(withIdentifier: "juegosId") as! JuegosViewController
        self.present(main, animated: true, completion: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func ParticipaClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let main = storyboard.instantiateViewController(withIdentifier: "participaId") as! ParticipaViewController
        self.present(main, animated: true, completion: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func PortalClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let main = storyboard.instantiateViewController(withIdentifier: "webviewId") as! WebViewViewController
        self.present(main, animated: true, completion: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    
    @IBAction func GacetitaClicked(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let main = storyboard.instantiateViewController(withIdentifier: "gacetitaId") as! GacetitaViewController
        self.present(main, animated: true, completion: nil)
        self.revealViewController().revealToggle(animated: true)
    }
    
    @IBOutlet weak var GrecaUpView: UIView!
    @IBOutlet weak var GrecaDownView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPatterns()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.GrecaUpView.frame.size)
        UIImage(named: "greco_header_up_small.png")?.drawAsPattern(in: self.GrecaUpView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.GrecaUpView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
        
        // Bottom pattern
        UIGraphicsBeginImageContext(self.GrecaDownView.frame.size)
        UIImage(named: "greco_header_down_small.png")?.drawAsPattern(in: self.GrecaDownView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.GrecaDownView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
}
