//
//  CategoriesViewCell.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 7/25/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class CategoriesViewCell: UICollectionViewCell {
    
    @IBOutlet weak var categoryImg: UIImageView!
    @IBOutlet weak var categoryTitle: UILabel!
}
