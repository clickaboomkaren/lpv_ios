//
//  CurioItemViewCell.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 9/4/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class CurioItemViewCell: UICollectionViewCell {

    @IBOutlet weak var answerTitle: UILabel!
    @IBOutlet weak var mBackground: UIView!
   
   override func awakeFromNib() {
      super.awakeFromNib()
      // Initialization code
      self.mBackground.backgroundColor = .clear
      self.mBackground.layer.borderWidth = 1
      let myColor: UIColor = UIColor( red: CGFloat(212/255.0), green: CGFloat(212/255.0), blue: CGFloat(212/255.0), alpha: CGFloat(1.0))
      self.mBackground.layer.borderColor = myColor.cgColor
   }
}
