//
//  NahuaItemViewCell.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 9/10/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class NahuaItemViewCell: UICollectionViewCell {

    @IBOutlet weak var nahuaImg: UIImageView!
    @IBOutlet weak var nahuaTitle: UILabel!
    @IBOutlet weak var checkedImg: UIImageView!
    @IBOutlet weak var circle: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.circle.backgroundColor = .clear
        self.circle.layer.cornerRadius = self.circle.frame.height/2
        self.circle.layer.borderWidth = 1
        let myColor: UIColor = UIColor( red: CGFloat(212/255.0), green: CGFloat(212/255.0), blue: CGFloat(212/255.0), alpha: CGFloat(1.0))
        self.circle.layer.borderColor = myColor.cgColor
    }
}
