//
//  CurioseandoMenuViewCell.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 9/3/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class CurioseandoMenuViewCell: UICollectionViewCell {
    @IBOutlet weak var gameTitle: UILabel!
    @IBOutlet weak var widthConstraint: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.contentView.translatesAutoresizingMaskIntoConstraints = false
        let screenWidth = UIScreen.main.bounds.size.width
        widthConstraint.constant = screenWidth - (2 * 12)
    }
}
