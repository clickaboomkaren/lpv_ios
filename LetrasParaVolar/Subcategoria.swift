//
//  Subcategoria.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/18/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import RealmSwift

final class Subcategoria: Object {
    dynamic var id = ""
    dynamic var nombre = ""
    dynamic var descripcion = ""
    dynamic var categoriasId = ""
}
