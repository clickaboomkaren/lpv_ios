//
//  BibliotecaViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 7/3/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Nuke
import RealmSwift

class BibliotecaViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    
    @IBOutlet weak var leyendasCollection: UICollectionView!
    @IBOutlet weak var coleccionesCollection: UICollectionView!
    @IBOutlet weak var subHeaderView: UIView!
    @IBOutlet weak var leyendasHeight: NSLayoutConstraint!
    @IBOutlet weak var coleccionesHeight: NSLayoutConstraint!
    @IBOutlet weak var emptyLegLabel: UILabel!
    @IBOutlet weak var emptyColLabel: UILabel!
    
    var leyendasItems:[Colecciones] = []
    var coleccionesItems:[Colecciones] = []
    
    // Order buttons
    @IBAction func orderBtnPressed(_ sender: UIButton) {
        restoreOrderColors()
        var params:String = ""
        
        switch sender.tag {
        case 1:
            params = "descargado"
            break
        case 2:
            params = "favorito"
            break
        default:
            params = "descargado"
        }
        
        print(params)
        
        // Set new color of pressed order button
        if let label = self.view.viewWithTag(sender.tag) as? UILabel {
            label.backgroundColor = UIColor( red: CGFloat(55/255.0), green: CGFloat(86/255.0), blue: CGFloat(188/255.0), alpha: CGFloat(1.0))
        }
        
        loadLocalBooks(order:params)
    }

    func restoreOrderColors() {
        for var tag in 1 ... 2 {
            if let label = self.view.viewWithTag(tag) as? UILabel {
                label.backgroundColor = UIColor( red: CGFloat(102/255.0), green: CGFloat(132/255.0), blue: CGFloat(226/255.0), alpha: CGFloat(1.0))
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPatterns()
        
        self.leyendasCollection.register(UINib(nibName: "BookViewCell", bundle: nil), forCellWithReuseIdentifier: "leyendasCell")
        self.coleccionesCollection.register(UINib(nibName: "BookViewCell", bundle: nil), forCellWithReuseIdentifier: "coleccionesCell")
        
        

    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadLocalBooks(order:"descargado")
        restoreOrderColors()
        // Set new color of pressed order button
        if let label = self.view.viewWithTag(1) as? UILabel {
            label.backgroundColor = UIColor( red: CGFloat(55/255.0), green: CGFloat(86/255.0), blue: CGFloat(188/255.0), alpha: CGFloat(1.0))
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
        UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.subHeaderView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    func loadLocalBooks(order: String) {
        leyendasItems.removeAll()
        coleccionesItems.removeAll()
        
        // Set empty text
        if order == "descargado" {
            self.emptyLegLabel.text = "No tienes leyendas descargadas"
            self.emptyColLabel.text = "No tienes libros descargados"
        } else {
            self.emptyLegLabel.text = "No tienes leyendas marcadas como favoritas"
            self.emptyColLabel.text = "No tienes libros marcados como favoritos"
        }
        
        // Get the default Realm
        let realm = try! Realm()
        
        // Query Realm for all downloaded books
        var itemsAr = realm.objects(Colecciones.self).filter(order + " = true AND bookType = 'leyendas'")
        if !itemsAr.isEmpty {
            leyendasItems.append(contentsOf: itemsAr)
            emptyLegLabel.isHidden = true
            leyendasCollection.isHidden = false
        } else {
            emptyLegLabel.isHidden = false
            leyendasCollection.isHidden = true
            // Set collectionView height acording to content
            leyendasHeight.constant = 15
            self.view.layoutIfNeeded()
        }
        
        // Query Realm for all downloaded books
        itemsAr = realm.objects(Colecciones.self).filter(order + " = true AND bookType = 'colecciones'")
        //colecciones
        if !itemsAr.isEmpty {
            coleccionesItems.append(contentsOf: itemsAr)
            emptyColLabel.isHidden = true
            coleccionesCollection.isHidden = false
        } else {
            emptyColLabel.isHidden = false
            coleccionesCollection.isHidden = true
            
            // Set collectionView height acording to content
            coleccionesHeight.constant = 15
            self.view.layoutIfNeeded()
        }
        
        leyendasCollection.reloadData()
        coleccionesCollection.reloadData()
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == leyendasCollection {
            return leyendasItems.count
        } else if collectionView == coleccionesCollection {
            return coleccionesItems.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == leyendasCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "leyendasCell", for: indexPath) as! LeyendasViewCell
            
            cell.bookImg.image = nil
            
            if(leyendasItems.count > 0){
                let element:Colecciones = leyendasItems[indexPath.row % leyendasItems.count]
                print(element)
                if element.imagenes.count > 0 {
                    let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + element.imagenes[0].imagen)!
                    Nuke.loadImage(with: url, into: cell.bookImg)
                
                    // Book title
                    cell.title.text = element.titulo as String?
                    
                    // Obtener autor
                    let autores = element.autores
                    let autor:Autores = autores[0]
                    cell.subtitle.text = autor.autor as String?
                }
            }
            
            // Set collectionView height acording to content
            let height:CGFloat = leyendasCollection.collectionViewLayout.collectionViewContentSize.height
            leyendasHeight.constant = height
            self.view.layoutIfNeeded()
            
            return cell
            
        } else if collectionView == coleccionesCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coleccionesCell", for: indexPath) as! LeyendasViewCell
            
            cell.bookImg.image = nil
            
            if(coleccionesItems.count > 0){
                let element:Colecciones = coleccionesItems[indexPath.row % coleccionesItems.count]
                print(element)
                if element.imagenes.count > 0 {
                    let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + element.imagenes[0].imagen)!
                    Nuke.loadImage(with: url, into: cell.bookImg)
                    
                    cell.title.text = element.titulo as String?
                    
                    // Obtener autor
                    let autores = element.autores
                    let autor:Autores = autores[0]
                    cell.subtitle.text = autor.autor as String?
                }
            }
            
            // Set collectionView height acording to content
            let height:CGFloat = coleccionesCollection.collectionViewLayout.collectionViewContentSize.height
            coleccionesHeight.constant = height
            self.view.layoutIfNeeded()
            
            return cell
            
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "coleccionesCell", for: indexPath) as! LeyendasViewCell
            
            cell.bookImg.image = nil
            
            if(coleccionesItems.count > 0){
                let element:Colecciones = coleccionesItems[indexPath.row % coleccionesItems.count]
                print(element)
                if element.imagenes.count > 0 {
                    let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + element.imagenes[0].imagen)!
                    Nuke.loadImage(with: url, into: cell.bookImg)

                    cell.title.text = element.titulo as String?
                    
                    // Obtener autor
                    let autores = element.autores
                    let autor:Autores = autores[0]
                    cell.subtitle.text = autor.autor as String?
                }
            }
            
            // Set collectionView height acording to content
            let height:CGFloat = coleccionesCollection.collectionViewLayout.collectionViewContentSize.height
            coleccionesHeight.constant = height
            self.view.layoutIfNeeded()
        
            return cell
        }
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      let height = self.view.frame.size.height
      let width = self.view.frame.size.width
        if collectionView == leyendasCollection || collectionView == coleccionesCollection {
         switch UIDevice.current.userInterfaceIdiom {
         case .phone:
            // It's an iPhone
            return CGSize(width: width/3.6, height: height*0.25)
         case .pad:
            // It's an iPad
            return CGSize(width: height*0.2, height: height*0.25)
         case .unspecified:
            // Uh, oh! What could it be?
            return CGSize(width: height*3.6, height: height*0.25)
         default:
            // default
            return CGSize(width: height*3.6, height: height*0.25)
         }
        } else {
            return CGSize(width: self.view.frame.width, height: collectionView.frame.size.height)
        }

    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == leyendasCollection || collectionView == coleccionesCollection {
            return 0
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == leyendasCollection {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "detallesId") as! BookDetailsViewController
            vc.mColType = "leyendas"
            vc.mItemId = leyendasItems[indexPath.row]["id"] as! String
         vc.mEpubRef = leyendasItems[indexPath.row]["epub"] as! String
            self.present(vc, animated: true, completion: nil)
        } else if collectionView == coleccionesCollection {
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "detallesId") as! BookDetailsViewController
            vc.mColType = "colecciones"
            vc.mItemId = coleccionesItems[indexPath.row]["id"] as! String
         vc.mEpubRef = coleccionesItems[indexPath.row]["epub"] as! String
            self.present(vc, animated: true, completion: nil)
        }
    }
    
}

