//
//  WebViewViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/16/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class WebViewViewController: UIViewController, UIWebViewDelegate {

    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        activityIndicator.color = UIColor.lightGray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        webView.frame = self.view.frame
        webView.scalesPageToFit = true
        
        let url = URL (string: "http://letrasparavolar.org/")
        let requestObj = URLRequest(url: url!)
        webView.loadRequest(requestObj)
        
        /*let button = UIButton(type: .system)
        button.setImage(UIImage(named: "rsz_backbtn"), for: .normal)
        button.setTitle("Atrás", for: .normal)
        button.addTarget(self, action: #selector(backNavBtnPressed(_:)), for: .touchUpInside)
        button.sizeToFit()
        self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: button)*/
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func webViewDidStartLoad(_ webView : UIWebView) {
    }
    
    func webViewDidFinishLoad(_ webView : UIWebView) {
        activityIndicator.stopAnimating()
        webView.updateLayoutForContent()
    }
}
