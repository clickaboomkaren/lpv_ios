//
//  Categoria.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/18/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import RealmSwift

final class Categoria: Object {
    dynamic var id = ""
    dynamic var nombre = ""
    dynamic var descripcion = ""
    dynamic var icono = ""
    var subcategorias = List<Subcategoria>()
    dynamic var categoriasColeccionesId = ""
    dynamic var categoria = ""
    dynamic var categoriasId = ""
    
    dynamic var active = false
    dynamic var categoryType = ""
}
