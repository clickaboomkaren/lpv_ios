//
//  ApiConfig.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 7/8/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import Foundation

class ApiConfig: NSObject {
    static let baseUrl = "http://app.letrasparavolar.org"
    
    // Banners
    static let banners = "/api/banners/"
    
    // Libros / Leyendas
    static let legends = "/api/libros/"
    static let legendsCategories = "/api/categorias/"
    static let legendsDefaults = "/api/libros/defaults/"
    static let searchLegends = "/api/search/libros/"
    
    // Colecciones
    static let collections = "/api/colecciones/"
    static let collectionsCategories = "/api/categoriascolecciones/"
    static let collectionsDefaults = "/api/colecciones/defaults/"
    static let searchCollections = "/api/search/colecciones/"
    
    // Images URL
    static let catImgPath = "/uploads/images/categorias/"
    static let collectionsImg = "/uploads/images/libros/thumb_"
    static let juegosImg = "/uploads/images/juegos/"
    
    // Epubs URL
    static let epubs = "/uploads/epubs/"
    
    // Internacionalización
    static let internacionalization = "/api/internacionalizacion/"
    
    // Mapa
    static let mapaMarkers = baseUrl + "/api/mapa/"
    
    // Games
    static let nahuatlismosGame = baseUrl + "/api/nahuatlismos/"
    static let curioseandoTests = baseUrl + "/api/curioseando/tests/"
    static let curioseandoTestQuestions = baseUrl + "/api/curioseando/preguntas/"
    static let curioseandoTestResult = baseUrl + "/api/curioseando/resultado"
    
    // Participa
    static let participa = "/api/participa/save/"
    
    // Noticias
    static let noticias = baseUrl + "/api/noticias/"
    
    // Gacetita
    static let gacetitas = baseUrl + "/api/gacetita/"
    static let gacetitaImg = baseUrl + "/uploads/images/pdfportadas/"
    static let gacetitaPdf = baseUrl + "/uploads/pdfs/"
    
    // Notificaciones
    static let registrarToken = baseUrl + "/api/Registrartoken/" //token, device_id
}
