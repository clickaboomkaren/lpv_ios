//
//  BookDetailsViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/1/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import Nuke
import RealmSwift

class BookDetailsViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    var mItemId:String = ""
    var mColType:String = ""
   var mEpubRef: String = ""
    var bookData:[String: Any] = [:]
    var mEpubItem = Colecciones()
    var url:String = ""
   var realm: Realm = try! Realm()
    
    @IBOutlet weak var GrecaUpView: UIView!
    @IBOutlet weak var GrecaDownView: UIView!
    
    @IBOutlet weak var bookImg: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var categoryTitleLbl: UILabel!
    @IBOutlet weak var catImg: UIImageView!
    
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var publisherLabel: UILabel!
    @IBOutlet weak var sizeLabel: UILabel!
    @IBOutlet weak var publishedLabel: UILabel!
    
    @IBOutlet weak var relatedCollection: UICollectionView!
    @IBOutlet weak var relatedHeight: NSLayoutConstraint!
    @IBOutlet weak var favoriteStarImg: UIImageView!
    @IBOutlet weak var downloadBtn: UIButton!
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func downloadBtnPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let main = storyboard.instantiateViewController(withIdentifier: "epubWebViewId") as! EpubBookContentViewController
        main.mEpubItem = mEpubItem
        self.present(main, animated: true, completion: nil)
    }
    
    @IBAction func favoriteBtnPressed(_ sender: UIButton) {
        // Qu2ery Realm for all downloaded books
        let itemsAr = realm.object(ofType: Colecciones.self, forPrimaryKey: self.mEpubItem.epub)
        if itemsAr != nil {
            if (itemsAr?.favorito)! {
                try! realm.write {
                    itemsAr?.favorito = false
                }
                favoriteStarImg.image = #imageLiteral(resourceName: "favorite_unselected")
            } else {
                try! realm.write {
                    itemsAr?.favorito = true
                }
                favoriteStarImg.image = #imageLiteral(resourceName: "favorite_selected")
            }
        } else {
            let alert = UIAlertController(title: "",    message: "Descárgalo para poder marcarlo como favorito", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setPatterns()
      
      // Get the default Realm
      realm = try! Realm()
      
        self.relatedCollection.register(UINib(nibName: "BookViewCell", bundle: nil), forCellWithReuseIdentifier: "leyendasCell")
        if mColType == "leyendas" {
            url = ApiConfig.legends
        } else {
            url = ApiConfig.collections
        }
    }

    override func viewDidAppear(_ animated: Bool) {
      // Load Book Data
      loadItem(url: url, params: "?id=" + mItemId)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.GrecaUpView.frame.size)
        UIImage(named: "greco_header_up_small.png")?.drawAsPattern(in: self.GrecaUpView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.GrecaUpView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
        
        // Bottom pattern
        UIGraphicsBeginImageContext(self.GrecaDownView.frame.size)
        UIImage(named: "greco_header_down_small.png")?.drawAsPattern(in: self.GrecaDownView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.GrecaDownView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    func loadItem(url:String, params: String) {
        bookData.removeAll()
        
        Alamofire.request(ApiConfig.baseUrl + url + params, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                     self.mEpubItem = Colecciones()
                     self.bookData = data[0] as! [String : Any]
                        
                        // Obtener imagenes del libro
                        let imagenList = List<Imagen>()
                        let imagenes:[[String: Any]] = (self.bookData["imagenes"] as? [[String : Any]])!
                        for imagen in imagenes {
                            let img = Imagen()
                            img.imagen = (imagen["imagen"] as? String)!
                            img.size = (imagen["imagen"] as? String)!
                            imagenList.append(img)
                        }
                        self.mEpubItem.imagenes = imagenList
                        
                        self.mEpubItem.titulo = (self.bookData["titulo"] as? String)!
                        
                        // Obtener categorías
                        let catList = List<Categoria>()
                        let categorias:[[String: Any]] = (self.bookData["categorias"] as? [[String : Any]])!
                        for categoria in categorias {
                            let cat = Categoria()
                            cat.icono = (categoria["icono"] as? String)!
                            cat.categoria = (categoria["categoria"] as? String)!
                            catList.append(cat)
                        }
                        self.mEpubItem.categorias = catList
                        
                        // Obtener autores
                        let autorList = List<Autores>()
                        let autores:[[String: Any]] = (self.bookData["autores"] as? [[String : Any]])!
                        for autor in autores {
                            let aut = Autores()
                            aut.autor = (autor["autor"] as? String)!
                            autorList.append(aut)
                        }
                        self.mEpubItem.autores = autorList
                        
                        if let descripcion = self.bookData["descripcion"] as? String {
                            self.mEpubItem.descripcion = descripcion
                        }
                        
                        if let editorial = self.bookData["editorial"] as? String {
                            self.mEpubItem.editorial = editorial
                        }
                        
                        if let fecha = self.bookData["fecha"] as? String {
                            self.mEpubItem.fecha = fecha
                        }
                        if let length = self.bookData["length"] as? String {
                            self.mEpubItem.length = length
                        }
                        
                        // Other data
                        if let id = self.bookData["id"] as? String {
                            self.mEpubItem.id = id
                        }
                        if let epub = self.bookData["epub"] as? String {
                            self.mEpubItem.epub = epub
                        }
                        if let creditos = self.bookData["creditos"] as? String {
                            self.mEpubItem.creditos = creditos
                        }
                        if let latitud = self.bookData["latitud"] as? String {
                            self.mEpubItem.latitud = latitud
                        }
                        if let longitud = self.bookData["longitud"] as? String {
                            self.mEpubItem.longitud = longitud
                        }
                        if let imagen = self.bookData["imagen"] as? String {
                            self.mEpubItem.imagen = imagen
                        }
                        if let coleccionesId = self.bookData["colecciones_id"] as? String {
                            self.mEpubItem.coleccionesId = coleccionesId
                        }
                     
                     // Get all answers for this question
                     let relacionadosList = List<Colecciones>()
                     if let respuestas = self.bookData["libros_relacionados"] as? [[String: Any]] {
                        for resp in respuestas {
                           let book = Colecciones()
                           
                           // Obtener imagenes del libro
                           /*let imagenList = List<Imagen>()
                           let imagenes:[[String: Any]] = (resp["imagenes"] as? [[String : Any]])!
                           for imagen in imagenes {
                              let img = Imagen()
                              img.imagen = (imagen["imagen"] as? String)!
                              img.size = (imagen["imagen"] as? String)!
                              imagenList.append(img)
                           }
                           book.imagenes = imagenList*/
                           
                           book.titulo = (resp["titulo"] as? String)!
                           
                           // Obtener categorías
                           let catList = List<Categoria>()
                           let categorias:[[String: Any]] = (resp["categorias"] as? [[String : Any]])!
                           for categoria in categorias {
                              let cat = Categoria()
                              cat.icono = (categoria["icono"] as? String)!
                              cat.categoria = (categoria["categoria"] as? String)!
                              catList.append(cat)
                           }
                           book.categorias = catList
                           
                           // Obtener autores
                           let autorList = List<Autores>()
                           let autores:[[String: Any]] = (resp["autores"] as? [[String : Any]])!
                           for autor in autores {
                              let aut = Autores()
                              aut.autor = (autor["autor"] as? String)!
                              autorList.append(aut)
                           }
                           book.autores = autorList
                           
                           if let descripcion = resp["descripcion"] as? String {
                              book.descripcion = descripcion
                           }
                           
                           if let editorial = resp["editorial"] as? String {
                              book.editorial = editorial
                           }
                           
                           if let fecha = resp["fecha"] as? String {
                              book.fecha = fecha
                           }
                           if let length = resp["length"] as? String {
                              book.length = length
                           }
                           
                           // Other data
                           if let id = resp["id"] as? String {
                              book.id = id
                           }
                           if let epub = resp["epub"] as? String {
                              book.epub = epub
                           }
                           if let creditos = resp["creditos"] as? String {
                              book.creditos = creditos
                           }
                           if let latitud = resp["latitud"] as? String {
                              book.latitud = latitud
                           }
                           if let longitud = resp["longitud"] as? String {
                              book.longitud = longitud
                           }
                           if let imagen = resp["imagen"] as? String {
                              book.imagen = imagen
                           }
                           if let coleccionesId = resp["colecciones_id"] as? String {
                              book.coleccionesId = coleccionesId
                           }
                           relacionadosList.append(book)
                        }
                        
                        self.mEpubItem.librosRelacionados = relacionadosList
                     } // END: Get all answers for this question
                        
                        self.mEpubItem.bookType = self.mColType
               
                           //self.realm.add(self.mEpubItem)
                           
                           // Inside here cause write is in another thread, so must wait until
                           // write is done to then update
                           DispatchQueue.main.async() {
                              self.updateUI()
                           }
                        //}
                     /*} else {
                        DispatchQueue.main.async() {
                           self.updateUI()
                        }
                     }*/
                  }
                }
                break
                
            case .failure(_):
                print(response.result.error)
                
                // Get the default Realm
                let realm = try! Realm()
                
                // Qu2ery Realm for all downloaded books
                let itemsAr = realm.object(ofType: Colecciones.self, forPrimaryKey: self.mEpubRef)
                if itemsAr != nil {
                  self.mEpubItem = itemsAr!
                  self.updateUI()
                }
                
                break
                
            }
        }
    }
    
    func updateUI() {
        
        // Obtener imagen libro
        if mEpubItem.imagenes.count > 0 {
            let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + mEpubItem.imagenes[0].imagen)!
            Nuke.loadImage(with: url, into: bookImg)
        }
        
        titleLabel.text = mEpubItem.titulo
        
        // Obtener categoría
        if mEpubItem.categorias.count > 0 {
            let comURL = ApiConfig.baseUrl + ApiConfig.catImgPath + mEpubItem.categorias[0].icono;
            let url = URL.init(string:comURL)!
            Nuke.loadImage(with: url, into: catImg) { [weak view] response, _ in
                let newImg = response.value?.withRenderingMode(.alwaysTemplate)
                self.catImg.tintColor = UIColor(red: CGFloat(207/255.0), green: CGFloat(54/255.0), blue: CGFloat(131/255.0), alpha: CGFloat(1.0))
                self.catImg.image = newImg
            }
            
            categoryTitleLbl.text = mEpubItem.categorias[0].categoria
            categoryLabel.text = mEpubItem.categorias[0].categoria
        }
    
        // Obtener autor
        if mEpubItem.autores.count > 0 {
            subtitleLabel.text = mEpubItem.autores[0].autor
        }
        dateLabel.text = mEpubItem.fecha
        
        descriptionLabel.text = mEpubItem.descripcion
    
        publisherLabel.text = mEpubItem.editorial
        publishedLabel.text = mEpubItem.fecha
        sizeLabel.text = mEpubItem.length + " MB"
      
      // Check if epub is locally stored
      print(self.mEpubItem.epub)
      let itemsAr = realm.object(ofType: Colecciones.self, forPrimaryKey: self.mEpubRef)
      if itemsAr != nil {
         if (itemsAr?.favorito)! {
            self.favoriteStarImg.image = #imageLiteral(resourceName: "favorite_selected")
         } else {
            self.favoriteStarImg.image = #imageLiteral(resourceName: "favorite_unselected")
         }
         
         //if (itemsAr?.descargado)! {
            self.downloadBtn.setTitle("Abrir", for: .normal)
         /*} else {
            self.downloadBtn.setTitle("Descargar", for: .normal)
         }*/
      } else {
         self.downloadBtn.setTitle("Descargar", for: .normal)
         /*try! self.realm.write {
            self.mEpubItem.descargado = false
            self.mEpubItem.favorito = false
            self.realm.add(self.mEpubItem)
         }**/
      }
      
      self.relatedCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
         return mEpubItem.librosRelacionados.count
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
      if collectionView == relatedCollection {
         let vc = self.storyboard!.instantiateViewController(withIdentifier: "detallesId") as! BookDetailsViewController
         vc.mColType = mColType
         vc.mItemId = mEpubItem.librosRelacionados[indexPath.row].id as! String
         self.present(vc, animated: true, completion: nil)
      }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath)
        //cell?.backgroundColor = UIColor( red: CGFloat(215/255.0), green: CGFloat(103/255.0), blue: CGFloat(161/255.0), alpha: CGFloat(1.0))
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == relatedCollection {
         let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "leyendasCell", for: indexPath) as! LeyendasViewCell
         
         cell.bookImg.image = nil
         
         if(self.mEpubItem.librosRelacionados.count > 0){
            let element = self.mEpubItem.librosRelacionados[indexPath.row]
            //print(element)
            if let imagen = element.imagen as? String {
               //print(image)
               let url = URL.init(string:ApiConfig.baseUrl + ApiConfig.collectionsImg + imagen)!
               Nuke.loadImage(with: url, into: cell.bookImg)
            }
            cell.title.text = element.titulo
            cell.subtitle.text = element.autores[0].autor
            
            // Set collectionView height acording to content
            let height:CGFloat = relatedCollection.collectionViewLayout.collectionViewContentSize.height
            relatedHeight.constant = height
            self.view.layoutIfNeeded()
            
         }
         return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "leyendasCell", for: indexPath) as! LeyendasViewCell
            return cell
        }
    }
    
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
      let height = self.view.frame.size.height
      let width = self.view.frame.size.width
      if collectionView == relatedCollection {
         switch UIDevice.current.userInterfaceIdiom {
         case .phone:
            // It's an iPhone
            return CGSize(width: width/3.6, height: height*0.25)
         case .pad:
            // It's an iPad
            return CGSize(width: height*0.15, height: height*0.25)
         case .unspecified:
            // Uh, oh! What could it be?
            return CGSize(width: height*3.6, height: height*0.25)
         default:
            // default
            return CGSize(width: height*3.6, height: height*0.25)
         }
      } else {
         return CGSize(width: self.view.frame.width, height: collectionView.frame.size.height)
      }
   }
   
   func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
      if collectionView == relatedCollection {
         return 10
      } else {
         return 0
      }
   }
}
