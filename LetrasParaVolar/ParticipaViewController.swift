//
//  ParticipaViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/16/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire

class ParticipaViewController: UIViewController {

    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func sendBtnPressed(_ sender: UIButton) {
        if(!(nameField.text?.isEmpty)!
            && !(mailField.text?.isEmpty)!
            && !(schoolField.text?.isEmpty)!
            && !(messageField.text?.isEmpty)!) {
            var params = "?nombre=" + nameField.text!
            params += "&email=" + mailField.text!
            params += "&escuela=" + schoolField.text!
            params += "&mensaje=" + messageField.text!
            saveForm(params: params)
        } else {
            let alert = UIAlertController(title: "",    message: "Todos los campos son requeridos", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @IBOutlet weak var subHeaderView: UIView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var mailField: UITextField!
    @IBOutlet weak var schoolField: UITextField!
    @IBOutlet weak var messageField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setPatterns()
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
        UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.subHeaderView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    func saveForm(params: String) {
        var url = ApiConfig.baseUrl + ApiConfig.participa + params
        url = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)!
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    DispatchQueue.main.async() {
                        let alert = UIAlertController(title: "",    message: "Se ha enviado tu mensaje", preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: { action in
                            switch action.style{
                                case .default:
                                    self.navigationController?.popViewController(animated: true)
                                    self.dismiss(animated: true, completion: nil)
                                
                                case .cancel:
                                    print("cancel")
                                
                                case .destructive:
                                    print("destructive")
                            }
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }

}
