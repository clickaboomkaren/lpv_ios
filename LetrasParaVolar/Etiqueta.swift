//
//  Etiqueta.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/18/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import RealmSwift

final class Etiqueta: Object {
    dynamic var etiqueta = ""
    dynamic var tagsId = ""
}
