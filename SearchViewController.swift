//
//  SearchViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 9/19/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController {

    @IBAction func outerSpaceBtn(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var GrecaUpView: UIView!
    @IBOutlet weak var GrecaDownView: UIView!
    var callerVC: UIViewController!
    var searchText: String! {didSet{
            dismiss(animated: true) {[weak self] in
                guard let strongSelf = self else {return}
                if let leyendasVC = strongSelf.callerVC as? LeyendasViewController {
                    leyendasVC.searchText = strongSelf.searchText
                } else if let coleccionesVC = strongSelf.callerVC as? ColeccionesViewController {
                    coleccionesVC.searchText = strongSelf.searchText
                }
        }
        }}
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        view.isOpaque = false
        
        setPatterns()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.GrecaUpView.frame.size)
        UIImage(named: "greco_header_up_small.png")?.drawAsPattern(in: self.GrecaUpView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.GrecaUpView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
        
        // Bottom pattern
        UIGraphicsBeginImageContext(self.GrecaDownView.frame.size)
        UIImage(named: "greco_header_down_small.png")?.drawAsPattern(in: self.GrecaDownView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.GrecaDownView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        searchText = textField.text!
        return false
    }
}
