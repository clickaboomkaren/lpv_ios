//
//  NoticiasViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/24/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import Nuke

class NoticiasViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }

    @IBOutlet weak var subHeaderView: UIView!
    @IBOutlet weak var noticiasCollection: UICollectionView!
    var noticiasItems:[[String: Any]] = []
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)

    override func viewDidLoad() {
        super.viewDidLoad()
        setPatterns()
        
        activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        activityIndicator.color = UIColor.lightGray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        self.noticiasCollection.register(UINib(nibName: "NoticiasViewCell", bundle: nil), forCellWithReuseIdentifier: "noticiaCell")
        if let flowLayout = noticiasCollection.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height:1)
        }
        
        loadNoticias()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
        UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.subHeaderView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    func loadNoticias() {
        noticiasItems = []
        
        Alamofire.request(ApiConfig.noticias + "?page=1", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                //print(response.result.value!)
                if let objJson = response.result.value as? [[String: Any]] {
                    weakself?.noticiasItems = objJson
                    DispatchQueue.main.async() {
                        UIApplication.shared.isNetworkActivityIndicatorVisible = false
                        self.activityIndicator.stopAnimating()
                        weakself?.noticiasCollection.reloadData()
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == noticiasCollection {
            return noticiasItems.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == noticiasCollection {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let main = storyboard.instantiateViewController(withIdentifier: "notiDetailsId") as! NoticiasDetailsViewController
            main.noticiaItem = noticiasItems[indexPath.row]
            self.present(main, animated: true, completion: nil)
            //self.revealViewController().revealToggle(animated: true)
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == noticiasCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "noticiaCell", for: indexPath) as! NoticiasViewCell
            
            //cell.notiImg.image = nil
            
            if(noticiasItems.count > 0){
                let element = noticiasItems[indexPath.row]
                print(element)
                /*if let imagen = element["image"] as? String {
                    let url = URL.init(string: imagen)!
                    Nuke.loadImage(with: url, into: cell.notiImg)
                } else {
                    cell.imgWidthConstraint.constant = 0
                }*/
                cell.titleLabel.text = element["title"] as? String
                cell.dateLabel.text = element["date"] as? String
                do {
                    let attrStr = try NSAttributedString(
                        data: ((element["description"] as? String)?.data(using: String.Encoding.unicode, allowLossyConversion: true)!)!,
                        options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                        documentAttributes: nil)
                    cell.descriptionLabel.attributedText = attrStr
                } catch let error {
                    
                }
                
                self.view.layoutIfNeeded()
                
            }
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "noticiaCell", for: indexPath) as! NoticiasViewCell
            return cell
        }
    }
    
    /*func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == noticiasCollection {
            return CGSize(width: collectionView.frame.size.width, height: 143.0)
        } else {
            return CGSize(width: self.view.frame.width, height: collectionView.frame.size.height)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        if collectionView == noticiasCollection {
            return 0
        } else {
            return 0
        }
    }*/
    
}
