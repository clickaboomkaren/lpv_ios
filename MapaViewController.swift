//
//  MapaViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 9/11/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import GoogleMaps

class MapaViewController: UIViewController, GMSMapViewDelegate {

    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    @IBOutlet weak var mapViewOnScreen: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let camera = GMSCameraPosition.camera(withLatitude: 20.926893, longitude: -101.9256694, zoom: 4.0) // MEXICO CENTER
        self.mapViewOnScreen.camera = camera
        
        loadMarkers()
    }
    
    func mapView(_ mapView: GMSMapView, didTapInfoWindowOf marker: GMSMarker) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let main = storyboard.instantiateViewController(withIdentifier: "detallesId") as! BookDetailsViewController
        main.mColType = "leyendas"
        if let item = marker.userData as? [String: Any] {
            main.mItemId = item["id"] as! String
        }
        self.present(main, animated: true, completion: nil)
    }
    
    func loadMarkers() {
        Alamofire.request(ApiConfig.mapaMarkers, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["leyendas"] as? NSArray {
                        for itemSet in data {
                            if let itemSet = itemSet as? [String: Any] {
                                self.createMarker(item: itemSet)
                            }
                        }
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }
    
    func createMarker(item: [String: Any]){
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: Double(item["latitud"] as! String)!, longitude: Double(item["longitud"] as! String)!)
        marker.groundAnchor = CGPoint(x: 0.5, y: 0.5);
        marker.title = item["titulo"] as! String
        marker.map = self.mapViewOnScreen
        marker.userData = item
    }
}
