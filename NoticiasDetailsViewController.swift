//
//  NoticiasDetailsViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/24/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Nuke

class NoticiasDetailsViewController: UIViewController {

    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var notiImg: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var imgHeightConstraint: NSLayoutConstraint!
    
    var noticiaItem:[String:Any] = [:]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateUI() {
        if let imagen = noticiaItem["image"] as? String {
            let url = URL.init(string: imagen)!
            Nuke.loadImage(with: url, into: notiImg)
        } else {
            imgHeightConstraint.constant = 0
        }
        titleLabel.text = noticiaItem["title"] as? String
        dateLabel.text = noticiaItem["date"] as? String
        do {
            let attrStr = try NSAttributedString(
                data: ((noticiaItem["description"] as? String)?.data(using: String.Encoding.unicode, allowLossyConversion: true)!)!,
                options: [ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType],
                documentAttributes: nil)
            descriptionLabel.attributedText = attrStr
        } catch let error {
            
        }

    }
}
