//
//  JuegosViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 8/27/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import Nuke

class JuegosViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    
    @IBOutlet weak var subHeaderView: UIView!
    @IBOutlet weak var juegosCollection: UICollectionView!
    var juegosItems:[Game] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPatterns()
        
        self.juegosCollection.register(UINib(nibName: "JuegosViewCell", bundle: nil), forCellWithReuseIdentifier: "juegoCell")
        if let flowLayout = juegosCollection.collectionViewLayout as? UICollectionViewFlowLayout {
            flowLayout.estimatedItemSize = CGSize(width: 1, height:1)
        }
        
        loadJuegos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
        UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.subHeaderView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    func loadJuegos() {
        juegosItems = []
        let game = Game()
        
        // Nahuatlismos
        game.id = "-1"
        game.title = "Nahuatlismos"
        game.subtitle = "Relaciona las palabras de origen prehispánico con su significado."
        game.imgResource = #imageLiteral(resourceName: "juego_1")
        game.gameType = "JUEGO_A"
        game.btnColor = UIColor( red: CGFloat(86/255.0), green: CGFloat(153/255.0), blue: CGFloat(183/255.0), alpha: CGFloat(1.0))
        juegosItems.append(game)
        
        // Curioseando
        let game2 = Game()
        game2.id = "-1"
        game2.title = "Curioseando"
        game2.subtitle = "Contesta los diferentes tests que tenemos para ti."
        game2.imgResource = #imageLiteral(resourceName: "juego_2")
        game2.gameType = "JUEGO_B"
        game2.btnColor = UIColor( red: CGFloat(185/255.0), green: CGFloat(151/255.0), blue: CGFloat(193/255.0), alpha: CGFloat(1.0))
        
        juegosItems.append(game2)
        
        juegosCollection.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == juegosCollection {
            return juegosItems.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == juegosCollection {
            if juegosItems[indexPath.row].gameType == "JUEGO_A" {
                // Nahuatlismos
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let main = storyboard.instantiateViewController(withIdentifier: "inGameId") as! InGameViewController
                main.mGame = juegosItems[0]
                self.present(main, animated: true, completion: nil)
                
            } else if juegosItems[indexPath.row].gameType == "JUEGO_B" {
                // Curioseando
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let main = storyboard.instantiateViewController(withIdentifier: "curioMenuId") as! CurioseandoMenuViewController
                main.mGame = juegosItems[1]
                self.present(main, animated: true, completion: nil)

            }
            
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == juegosCollection {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "juegoCell", for: indexPath) as! JuegosViewCell
            let game:Game = juegosItems[indexPath.row]
            cell.gameImg.image = game.imgResource
            cell.titleLabel.text = game.title
            cell.descriptionLabel.text = game.subtitle
           
            self.view.layoutIfNeeded()
            
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "juegoCell", for: indexPath) as! JuegosViewCell
            return cell
        }
    }

}
