//
//  InGameViewController.swift
//  LetrasParaVolar
//
//  Created by Clickaboom on 9/3/17.
//  Copyright © 2017 Clickaboom. All rights reserved.
//

import UIKit
import Alamofire
import Nuke
import Social

class InGameViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    @IBAction func backBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
    }
    @IBAction func nextBtnPressed(_ sender: UIButton) {
        if mQuestionIndex < mPregList.count - 1 {
            mQuestionIndex += 1
            setQuestion(pregunta: mPregList[mQuestionIndex])
        } else {
            modalGameOver.isHidden = false
            if mGame.gameType == "JUEGO_A" {
                resNahuaPuntua.text = String(mCorrectCounter)
            } else {
                mSelectedCell.mBackground.backgroundColor = UIColor( red: CGFloat(255/255.0), green: CGFloat(255/255.0), blue: CGFloat(255/255.0), alpha: CGFloat(1.0))
                loadCurioseandoResult(resId: getResultId())
            }
        }
    }
    @IBAction func repeatBtnPressed(_ sender: UIButton) {
        if mGame.gameType == "JUEGO_A" {
            mCorrectCounter = 0
            mQuestionIndex = 0
            modalGameOver.isHidden = true
            setQuestion(pregunta: mPregList[mQuestionIndex])
        } else {
            navigationController?.popViewController(animated: true)
            dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func finishBtnPressed(_ sender: UIButton) {
        navigationController?.popViewController(animated: true)
        dismiss(animated: true, completion: nil)
 
    }
    @IBAction func shareBtnPressed(_ sender: UIButton) {
        var shareView: UIView
    
        if mGame.gameType == "JUEGO_A" {
            shareView = self.resCurioView
        } else {
            shareView = self.resNahuaView
        }
        shareView = self.view
        
        /*//Create the UIImage
        UIGraphicsBeginImageContext(shareView.frame.size)
        shareView.layer.render(in: UIGraphicsGetCurrentContext()!)
        
        // image to share
        let image = UIGraphicsGetImageFromCurrentImageContext()
        //image = resizeImage(image: image!, newWidth: 200)
        UIGraphicsEndImageContext()
    
        
        // set up activity view controller
        let imageToShare = [ image! ]
        let activityViewController = UIActivityViewController(activityItems: imageToShare, applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view // so that iPads won't crash
        
        // exclude some activity types from the list (optional)
        activityViewController.excludedActivityTypes = [ UIActivityType.airDrop, UIActivityType.postToFacebook ]
        
        // present the view controller
        self.present(activityViewController, animated: true, completion: nil)*/
        
        // to create an image from screenshot
        backBtnView.isHidden = true
        UIGraphicsBeginImageContext(shareView.frame.size)
        shareView.layer.render(in: UIGraphicsGetCurrentContext()!)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        backBtnView.isHidden = false
        
        // to share the image
        var imagesToShare = [AnyObject]()
        imagesToShare.append(image!)
      let url = URL(string: "fb://")!
      let isInstalled:Bool = UIApplication.shared.canOpenURL(url)
      if (isInstalled) {
         // Share image on Fb
         let vc = SLComposeViewController(forServiceType:SLServiceTypeFacebook)
         vc?.add(image)
         self.present(vc!, animated: true, completion: nil)
      } else {
         let alert = UIAlertController(title: nil, message: "Se requiere la aplicación de Facebook", preferredStyle: UIAlertControllerStyle.alert)
         alert.addAction(UIAlertAction(title: "Aceptar", style: UIAlertActionStyle.default, handler: { action in
            switch action.style{
            default:
               self.dismiss(animated: true, completion: nil)
               UIApplication.shared.openURL(URL(string: "itms://itunes.apple.com/app/id284882215")!)
               break
            }}))
         self.present(alert, animated: true, completion: nil)
      }
    }
    
    @IBOutlet weak var navTitleLabel: UILabel!
    @IBOutlet weak var backBtnView: UIView!
    
    @IBOutlet weak var questionTitleTxt: UILabel!
    @IBOutlet weak var subHeaderView: UIView!
    @IBOutlet weak var juegosCollection: UICollectionView!
    @IBOutlet weak var mNextBtn: UIButton!
    @IBOutlet weak var modalGameOver: UIView!
    
    @IBOutlet weak var resCurioView: UIView!
    @IBOutlet weak var resCurioImg: UIImageView!
    @IBOutlet weak var resCurioTitle: UILabel!
    @IBOutlet weak var resCurioDescription: UILabel!
    
    @IBOutlet weak var resNahuaView: UIView!
    @IBOutlet weak var resNahuaPuntua: UILabel!
    
    
    let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: UIActivityIndicatorViewStyle.whiteLarge)
    var mPregList = [Pregunta]()
    var mRespList = [Respuesta]()
    var mGame = Game()
    var mAnswerClicked = false
    var mQuestionIndex = 0
    var mCorrectCounter = 0
    var mCurioAnswers:[Int:Int] = [:]
    var mSelectedCell = CurioItemViewCell()
    var mNahuaSelCell = NahuaItemViewCell()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setPatterns()
        
        activityIndicator.center = CGPoint(x: view.bounds.size.width/2, y: view.bounds.size.height/2)
        activityIndicator.color = UIColor.lightGray
        view.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        invalidNextBtn()
        
        if(mGame.gameType == "JUEGO_A") {
            navTitleLabel.text = "Nahuatlismos"
            loadNahuatlismosQuestions()
            resNahuaView.isHidden = false
            self.juegosCollection.register(UINib(nibName: "NahuaItemViewCell", bundle: nil), forCellWithReuseIdentifier: "nahuaCell")
        } else {
            navTitleLabel.text = "Curioseando"
            loadCurioseandoQuestions(testId: mGame.id)
            resCurioView.isHidden = false
            self.juegosCollection.register(UINib(nibName: "CurioItemViewCell", bundle: nil), forCellWithReuseIdentifier: "curioCell")
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setPatterns() {
        // Top pattern
        UIGraphicsBeginImageContext(self.subHeaderView.frame.size)
        UIImage(named: "sub_header_back_small.png")?.drawAsPattern(in: self.subHeaderView.bounds)
        
        if let image: UIImage = UIGraphicsGetImageFromCurrentImageContext(){
            UIGraphicsEndImageContext()
            self.subHeaderView.backgroundColor = UIColor(patternImage: image)
        }else{
            UIGraphicsEndImageContext()
            debugPrint("Image not available")
        }
    }
    
    // MARK: loadNahuatlismosQuestions
    func loadNahuatlismosQuestions() {
        Alamofire.request(ApiConfig.nahuatlismosGame, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                        for itemSet in data {
                            let preg = Pregunta()
                            if let itemSet = itemSet as? [String: Any] {
                                if let val = itemSet["id"] as? String {
                                    preg.id = val
                                }
                                if let val = itemSet["pregunta"] as? String {
                                    preg.pregunta = val
                                }
                                // Get all answers for this question
                                var respuestasList = [Respuesta]()
                                if let respuestas = itemSet["respuestas"] as? [[String: Any]] {
                                    for resp in respuestas {
                                        let respuesta = Respuesta()
                                        
                                        if let val = resp["id"] as? String {
                                            respuesta.id = val
                                        }
                                        if let val = resp["imagen"] as? String {
                                            respuesta.imagen = val
                                        }
                                        if let val = resp["is_correcta"] as? String {
                                            respuesta.isCorrecta = val
                                        }
                                        if let val = resp["preguntas_nahuatlismos_id"] as? String {
                                            respuesta.preguntasCurioseandoId = val
                                        }
                                        if let val = resp["respuesta"] as? String {
                                            respuesta.respuesta = val
                                        }
                                        
                                        respuestasList.append(respuesta)
                                    }
                                
                                    preg.respuestas = respuestasList
                                } // END: Get all answers for this question

                                //weakself?.preguntasItems.append(itemSet)
                            }
                            self.mPregList.append(preg)
                        }
                        
                        DispatchQueue.main.async() {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.activityIndicator.stopAnimating()
                            self.setQuestion(pregunta: self.mPregList[self.mQuestionIndex])
                        }
                    }
                }
                break

            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }

    // MARK: loadCurioseandoQuestions
    func loadCurioseandoQuestions(testId: String) {
        Alamofire.request(ApiConfig.curioseandoTestQuestions + "?test=" + testId, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                        for itemSet in data {
                            let preg = Pregunta()
                            if let itemSet = itemSet as? [String: Any] {
                                if let val = itemSet["id"] as? String {
                                    preg.id = val
                                }
                                if let val = itemSet["pregunta"] as? String {
                                    preg.pregunta = val
                                }
                                // Get all answers for this question
                                var respuestasList = [Respuesta]()
                                if let respuestas = itemSet["respuestas"] as? [[String: Any]] {
                                    for resp in respuestas {
                                        let respuesta = Respuesta()
                                        if let val = resp["id"] as? String {
                                            respuesta.id = val
                                        }
                                        if let val = resp["preguntas_curioseando_id"] as? String {
                                            respuesta.preguntasCurioseandoId = val
                                        }
                                        if let val = resp["respuesta"] as? String {
                                            respuesta.respuesta = val
                                        }
                                        // Get all results for this answer
                                        var resultadosList = [Resultado]()
                                        if let resultados = resp["resultados"] as? [[String: Any]] {
                                            for result in resultados {
                                                let resultado = Resultado()
                                                if let val = result["id"] as? String {
                                                    resultado.id = val
                                                }
                                                if let val = result["respuestas_curioseando_id"] as? String {
                                                    resultado.respuestasCurioseandoId = val
                                                }
                                                if let val = result["resultados_curioseando_id"] as? String {
                                                    resultado.resultadosCurioseandoId = val
                                                }
                                                if let val = result["valor"] as? String {
                                                    resultado.valor = val
                                                }
                                                resultadosList.append(resultado)
                                            }
                                            respuesta.resultados.append(contentsOf: resultadosList)
                                        } // END: Get all results for this answer
                                        respuestasList.append(respuesta)
                                    }
                                    preg.respuestas.append(contentsOf: respuestasList)
                                } // END: Get all answers for this question
                                //weakself?.preguntasItems.append(itemSet)
                            }
                            self.mPregList.append(preg)
                        }
                        
                        DispatchQueue.main.async() {
                            UIApplication.shared.isNetworkActivityIndicatorVisible = false
                            self.activityIndicator.stopAnimating()
                            self.setQuestion(pregunta: self.mPregList[self.mQuestionIndex])
                        }
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }
    
    func loadCurioseandoResult(resId: Int) {
        let url = ApiConfig.curioseandoTestResult + "?resultado=\(resId)"
        Alamofire.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON {[weak weakself = self](response:DataResponse<Any>) in
            
            switch(response.result) {
            case .success(_):
                print(response.result.value!)
                if let objJson = response.result.value as? [String: Any] {
                    if let data = objJson["data"] as? NSArray {
                        for itemSet in data {
                            if let element = itemSet as? [String: Any] {
                                //let element:[String:Any] = itemSet[0]
                                // Image
                                if let imagen = element["imagen"] as? String {
                                    //print(image)
                                    let url = ApiConfig.baseUrl + ApiConfig.juegosImg + "thumb_" + imagen
                                    //let imgUrl = ApiConfig.baseUrl + ApiConfig.juegosImg + "thumb_" + element.imagen
                                    Nuke.loadImage(with: URL.init(string: url)!, into: self.resCurioImg)
                                }
                                
                                // Title
                                self.resCurioTitle.text = element["resultado"] as? String
                                
                                // Description
                                let desc = element["descripcion"] as? String
                                do {
                                    //self.resCurioDescription.text = try desc?.convertHtmlSymbols()
                                    self.resCurioDescription.text = try desc?.convertHtmlSymbols()
                                } catch let error {
                                    print("html error \(error)")
                                }
                            }
                        }
                    }
                }
                break
                
            case .failure(_):
                print(response.result.error)
                break
                
            }
        }
    }
    
    func getResultId() -> Int {
        var maxValue = 0
        var maxKey = 0
        for (key, value) in mCurioAnswers {
            if value > maxValue {
                maxValue = value
                maxKey = key
                print("\(key) , \(value)")
            }
        }
        return maxKey
    }
    
    func setQuestion(pregunta: Pregunta) {
        questionTitleTxt.text = pregunta.pregunta
        mRespList.removeAll()
        mRespList = pregunta.respuestas
        mAnswerClicked = false
        invalidNextBtn()
        juegosCollection.reloadData()
    }
    
    func invalidNextBtn() {
        mNextBtn.isEnabled = false
        mNextBtn.backgroundColor = UIColor( red: CGFloat(183/255.0), green: CGFloat(183/255.0), blue: CGFloat(183/255.0), alpha: CGFloat(1.0))
    }
    
    func validNextBtn(btnColor: UIColor) {
        mNextBtn.isEnabled = true
        mNextBtn.backgroundColor = btnColor
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == juegosCollection {
            return mRespList.count
        } else {
            return 0
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        validNextBtn(btnColor: mGame.btnColor);
        if mGame.gameType == "JUEGO_A" { // NAHUATLISMOS
            // If there hasn't been chosen an answer before, show the value
            if(!mAnswerClicked) {
                let cell = juegosCollection.cellForItem(at: indexPath)! as! NahuaItemViewCell
                mNahuaSelCell = cell
                mAnswerClicked = true
                var corrPos = 0;
                if mRespList[indexPath.row].isCorrecta == "SI" {
                    cell.checkedImg.image = #imageLiteral(resourceName: "checked")
                } else {
                    // Show that user has done wrong, and show him the
                    // correct answer
                    cell.checkedImg.image = #imageLiteral(resourceName: "unchecked")
                    for var i in (0 ... mRespList.count - 1) {
                        if mRespList[i].isCorrecta == "SI" {
                            corrPos = i
                        }
                    }
                }
                OnItemClicked(res: mRespList[indexPath.row], correctPos:corrPos)
            }
        } else if mGame.gameType == "JUEGO_B" { // CURIOSEANDO
            // If there hasn't been chosen an answer before, show the value
            if(!mAnswerClicked) {
                let cell = juegosCollection.cellForItem(at: indexPath)! as! CurioItemViewCell
                mSelectedCell = cell
                mAnswerClicked = true
                cell.mBackground.backgroundColor = UIColor( red: CGFloat(212/255.0), green: CGFloat(212/255.0), blue: CGFloat(212/255.0), alpha: CGFloat(1.0))
                OnItemClicked(res: mRespList[indexPath.row], correctPos:indexPath.row)
            }
        }
    }
    
    func OnItemClicked(res: Respuesta, correctPos:Int) {
        validNextBtn(btnColor: mGame.btnColor);
        if mGame.gameType == "JUEGO_A" {
            if res.isCorrecta == "SI" {
                mCorrectCounter += 1
            } else {
                let cell = juegosCollection.cellForItem(at: IndexPath(row: correctPos, section: 0))! as! NahuaItemViewCell
                cell.checkedImg.image = #imageLiteral(resourceName: "checked")
            }
        } else if mGame.gameType == "JUEGO_B" {
            // If there hasn't been chosen an answer before, show the value
            for answer:Resultado in res.resultados {
                var newValue = 0
                let resId = Int(answer.resultadosCurioseandoId)!
                if mCurioAnswers[resId] != nil{
                    newValue = mCurioAnswers[resId]! + Int(answer.valor)! // Sum previous value with new retrieved value
                }
                mCurioAnswers[resId] = newValue
            }
        }
        self.view.layoutIfNeeded()
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView == juegosCollection {
            if mGame.gameType == "JUEGO_A" {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "nahuaCell", for: indexPath) as! NahuaItemViewCell
            
                if(mRespList.count > 0){
                    let element:Respuesta = mRespList[indexPath.row]
                    
                    // Image
                    let imgUrl = ApiConfig.baseUrl + ApiConfig.juegosImg + "thumb_" + element.imagen
                    Nuke.loadImage(with: URL.init(string: imgUrl)!, into: cell.nahuaImg)
                    
                    // Title
                    do {
                        cell.nahuaTitle.text = try element.respuesta.convertHtmlSymbols()
                    } catch let error {
                    
                    }
                    
                    // Reset checked
                    cell.checkedImg.image = nil
                    
                    // Refresh cell
                    self.view.layoutIfNeeded()
                }
                return cell
            } else {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "curioCell", for: indexPath) as! CurioItemViewCell
                
                if(mRespList.count > 0){
                    let element:Respuesta = mRespList[indexPath.row]
                    do {
                        cell.answerTitle.text = try element.respuesta.convertHtmlSymbols()
                    } catch let error {
                        
                    }
                    
                    // Reset background 
                    cell.mBackground.backgroundColor = #colorLiteral(red: 0.9999960065, green: 1, blue: 1, alpha: 1)
                    
                    // Refresh cell
                    self.view.layoutIfNeeded()
                }
                return cell

            }
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "curioCell", for: indexPath) as! CurioItemViewCell
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if mGame.gameType == "JUEGO_A" {
            return CGSize(width: collectionView.bounds.width/2.1, height: collectionView.bounds.height/2.15)
        } else {
            return CGSize(width: collectionView.bounds.width/2.1, height: collectionView.bounds.height/3.15)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 8
    }
}

extension String {
    func convertHtmlSymbols() throws -> String? {
        guard let data = data(using: .utf8) else { return nil }
        
        return try NSAttributedString(data: data, options: [NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil).string
    }
}
